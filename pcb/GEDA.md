## gEDA

### Numbering elements
Attributes -> Autonumber.

### Footprints library
In `pcb`, Windows -> Library. Also, look on gedasymbols.org.

### How to make a symbol?
In gschem (or lepton-schematic), Add Pins, Lines, etc. Then Edit -> Symbol translate. When saving, select Symbol file type.

### Scheme to PCB
gsch2pcb -d footprints voltammeter.sch

### SO pads gap
Press ":" and type MinClearGap(0.25, mm)

### Vias size
Select -> Change size ...

### For printer toner transfer, do not forget the Mirror option
File -> Export... -> PS. Select "Mirror" option.

### Box with title, version, ...
The box is title-B.sym. See: http://wiki.geda-project.org/geda:gschem_warmup

* http://wiki.geda-project.org/geda:gsch2pcb_tutorial
* http://hobby-electrons.sourceforge.net/tutorials/gEDA/index.html#Ground%20pours
* http://pcb.geda-project.org/pcb-cvs/pcb.html
* http://www.gedasymbols.org
