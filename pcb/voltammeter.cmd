# Pin name action command file

# Start of element Q1
ChangePinName(Q1, 2, S)
ChangePinName(Q1, 3, D)
ChangePinName(Q1, 1, G)

# Start of element U3
ChangePinName(U3, 4, V-)
ChangePinName(U3, 8, V+)
ChangePinName(U3, 1, OUT)
ChangePinName(U3, 2, IN-)
ChangePinName(U3, 3, IN+)
ChangePinName(U3, 4, V-)
ChangePinName(U3, 8, V+)
ChangePinName(U3, 7, OUT)
ChangePinName(U3, 6, IN-)
ChangePinName(U3, 5, IN+)

# Start of element R3
ChangePinName(R3, 1, 1)
ChangePinName(R3, 2, 2)

# Start of element S1
ChangePinName(S1, 6, IN)
ChangePinName(S1, 7, GND)
ChangePinName(S1, 8, ON)
ChangePinName(S1, 4, V+)
ChangePinName(S1, 1, COM)

# Start of element R1
ChangePinName(R1, 1, 1)
ChangePinName(R1, 2, 2)

# Start of element R2
ChangePinName(R2, 1, 1)
ChangePinName(R2, 2, 2)

# Start of element C1
ChangePinName(C1, 2, -)
ChangePinName(C1, 1, +)

# Start of element C2
ChangePinName(C2, 2, -)
ChangePinName(C2, 1, +)

# Start of element C4
ChangePinName(C4, 2, 2)
ChangePinName(C4, 1, 1)

# Start of element R6
ChangePinName(R6, 1, 1)
ChangePinName(R6, 2, 2)

# Start of element R5
ChangePinName(R5, 1, 1)
ChangePinName(R5, 2, 2)

# Start of element C5
ChangePinName(C5, 2, 2)
ChangePinName(C5, 1, 1)

# Start of element R4
ChangePinName(R4, 1, 1)
ChangePinName(R4, 2, 2)

# Start of element C3
ChangePinName(C3, 2, 2)
ChangePinName(C3, 1, 1)

# Start of element U2
ChangePinName(U2, 4, V-)
ChangePinName(U2, 8, V+)
ChangePinName(U2, 7, OUT)
ChangePinName(U2, 6, IN-)
ChangePinName(U2, 5, IN+)
ChangePinName(U2, 4, V-)
ChangePinName(U2, 8, V+)
ChangePinName(U2, 1, OUT)
ChangePinName(U2, 2, IN-)
ChangePinName(U2, 3, IN+)

# Start of element C6
ChangePinName(C6, 2, -)
ChangePinName(C6, 1, +)

# Start of element U1
ChangePinName(U1, 16, D13)
ChangePinName(U1, 17, 3V3)
ChangePinName(U1, 18, AREF)
ChangePinName(U1, 19, A7)
ChangePinName(U1, 20, A6)
ChangePinName(U1, 21, A5)
ChangePinName(U1, 22, A4)
ChangePinName(U1, 23, A3)
ChangePinName(U1, 24, A2)
ChangePinName(U1, 25, A1)
ChangePinName(U1, 26, A0)
ChangePinName(U1, 27, +5V)
ChangePinName(U1, 28, RESET)
ChangePinName(U1, 29, GND)
ChangePinName(U1, 30, VIN)
ChangePinName(U1, 15, D12)
ChangePinName(U1, 14, D11~)
ChangePinName(U1, 13, D10~)
ChangePinName(U1, 12, D9~)
ChangePinName(U1, 11, D8)
ChangePinName(U1, 10, D7)
ChangePinName(U1, 9, D6~)
ChangePinName(U1, 8, D5~)
ChangePinName(U1, 7, D4)
ChangePinName(U1, 6, D3~)
ChangePinName(U1, 5, D2)
ChangePinName(U1, 4, GND)
ChangePinName(U1, 3, RESET)
ChangePinName(U1, 2, D0/RX)
ChangePinName(U1, 1, D1/TX)
