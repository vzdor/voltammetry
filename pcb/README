## Schematic/PCB

![Schematic](voltammeter.png)

Schematic and PCB is done in gEDA (gnu/free).

Can be powered from battery by connecting to Vin. If you decide to do so, no need to solder USB supply filter/limiter: Q1, R1, R2, C1, C2.

You do not have to solder S1 and R3 if you do not need switching maximum current ability, the mils= option.

### Filtering

WE current ADC channel can be selected with `--fc n`. `n` can be 3 or 4. The filter can be added between A3 and A4 pins.

### Etching

15 ml 3% hydrogen peroxide, 25 ml 9% acetic acid, 1/4 of small spoon of NaCl. PCB should be under 1-2 cm of liquid. Stir the pot.

* http://quinndunki.com/blondihacks/?p=835

### gEDA

All lines 0.3 mm. Min, clear gap 0.38 mm.

[gEDA Notes](GEDA.md)

## Micro controllers for simple PCB design

### ATxmega128/64/32/16A4U

The micro controller includes 12 bit ADC, DAC and USB (USB-UART micro-scheme is not required). May be crystal oscillator filter is not required. Not sure, but it seems the micro controller is pre-programmed with DFU bootloader so it can be programmed from computer USB directly.

And so it is less to solder and simpler PCB could be designed. Although manufacturer provides "standalone" library called ASF, it is a big package and I could not figure how to use only USB-CDC library. Their Windows IDE could be used to do this. It is 3.6 v max, so maximum voltage window or current (TIA) will be further limited.

https://github.com/dfu-programmer/dfu-programmer

### SAM D 11/21

ARM. 12 bit ADC, DAC, USB. SAM D11 is only 20 pins. Seems it does not have pre-programmed USB bootloader. But instead of ASF, the micro is supported by tinyUSB library. 3.6 v max.

https://github.com/hathach/tinyusb


### STM32

ARM. Some of them include USB, 12 bit ADC, DFU pre-programmed. Supported by tinyusb. Supported by libopencm3. libopencm3 supports USB (there is USB-CDC example) with STM32F1.

STM32F0x2 - crystal-less. 3.6 v. 20 pins.

Micro controllers with DFU bootloader: Application note AN3156.

https://libopencm3.org/

Book: Beginning STM32 (libopencm3 and FreeRTOS). Library does not rely on HAL.

### ATMEGA32U4

USB, 10 bit ADC. Pre-programmed DFU, so no need in programmer-device. It seems it can work with internal oscillator, although in low speed mode. 5 v. Unlike with previous micro controllers, very little changes to micro controller code will be required. Very small USB library, good for beginner studying USB.

The USB library: http://medesign.seas.upenn.edu/index.php/Guides/MaEvArM-usb

### ATTINY3226

No USB, internal oscillator, 12 bit ADC, 5 v, 20 pins. Can be programmed by UPDI, 1 pin (RESET pin), using USB to serial converter. ADC: up to 1024 samples accomulator. Programmable up to 16x amplifier (PGA).

For development, use two USB-serial converters, one for programming and one for UART.

8 pin USB to serial IC CH330N converter can be built with just a few components: https://github.com/wagiminator/AVR-Programmer

UPDI utility: https://github.com/mraardvark/pyupdi
