
#include "UART.h"

void UART_init() {
    // Upper and lower bytes of the calculated prescaler value for baud.
    UBRR0H = UBRRH_VALUE;
    UBRR0L = UBRRL_VALUE;
    // Configure data frame size to 8-bits.
    UCSR0C = _BV(UCSZ01) | _BV(UCSZ00);
    // Configure to enable transmitter and receiver.
    UCSR0B = _BV(TXEN0) | _BV(RXEN0);
}

int UART_put(char c, FILE *s) {
    // Wait until the register to write to is free.
    loop_until_bit_is_set(UCSR0A, UDRE0);
    // Write the byte to the register.
    UDR0 = c;
    return 0;
}

int UART_any() {
    return bit_is_set(UCSR0A, RXC0);
}

int UART_get(FILE *s) {
    loop_until_bit_is_set(UCSR0A, RXC0);
    return UDR0;
}
