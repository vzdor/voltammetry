require_relative 'experiment'
require_relative 'arguments'
require_relative 'simple_options'

scheme = Arguments.defaults { {dc: dc(v: 0, delay: 5)} }
options = simple_options scheme
experiment(options) { dc options[:dc] }
