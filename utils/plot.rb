class Plot
  class Gp
    class << self
      def quote(s)
        "\"#{s}\""
      end
    end
  end

  module SetColor
    def color=(s)
      self.lc = "rgb '#{s}'"
    end
  end

  class Settings < Hash
    class Terminal
      attr_accessor :type, :w, :h, :font, :etc

      def initialize
        @etc = [:noraise]
        # @type = :qt
        # qt term ignores fonts.conf minimum font size setting.
        @type = :wxt
        # It was mentioned that wxt term does not show
        # "slow font initialization..." problem. It has q key binded
        # to quit. It will scale the image if there is no enough
        # space.
        @w = 512
        @h = 384
        @font = 'Monospace,11'
      end

      def to_s(options = {})
        ary = [
          options[:terminal] || type
        ]
        unless options[:terminal]
          ary << [:size, [w, h].join(',')] if h && w
          ary << etc
        end
        ary << [:font, Gp.quote(options[:font] || font)]
        ary.join ' '
      end
    end

    class Linetype < Struct.new(:lc, :lw, :pt, :dt)
      include SetColor

      def set(**kw)
        kw.each { |k, v| send "#{k}=", v }
      end

      def to_s
        members.map do |k|
          v = send k
          [k, v] if v
        end.compact.join ' '
      end
    end

    attr_reader :terminal, :linetypes

    def initialize
      set :grid, 'back'
      @terminal = Terminal.new
      @linetypes = {}
    end

    def to_s(options = {})
      ary = *self
      from_options options, &ary.method(:push)
      ary << [:terminal, @terminal.to_s(options)]
      @linetypes.each { |i, lt| ary << [:linetype, i, lt] }
      ary.map { |el| ['set', el].join ' ' }.join "\n"
    end

    def linetype(i, **kw)
      linetype = @linetypes[i]
      unless linetype
        linetype = Linetype.new
        @linetypes[i] = linetype
      end
      linetype.set **kw
    end

    def set(k, *vals)
      self[k.to_sym] = vals.join ' '
    end

    # Two dt's per color.
    # Colors from:
    #  https://github.com/aschn/gnuplot-colorbrewer
    def colors_16
      colors = [
        '#E41A1C', '#377EB8', '#4DAF4A', '#984EA3',
        '#FF7F00', '#FFFF33', '#A65628', '#F781BF'
      ]
      8.times do |i|
        linetype i * 2 + 1, color: colors[i]
        linetype i * 2 + 2, color: colors[i], dt: 2
      end
    end

    protected

    def from_options(options, &block)
      [:output, :xtics, :ytics].each do |k|
        v = options[k]
        yield [k, v] if v
      end
      [:xlabel, :ylabel].each do |k|
        v = options[k]
        yield [k, Gp.quote(v)] if v
      end
      yield *options[:commands] if options.has_key? :commands
    end
  end

  attr_reader :parys, :converters, :settings

  # Command line plot arguments, which will be passed as options,
  # will have priority over plot.settings.
  def initialize(options = {})
    @options = options
    @settings = Settings.new
    @parys = []
    @converters = []
  end

  def new_pary
    pary = self.class.new_pary
    @parys << pary
    pary
  end

  def pary
    @parys.last
  end

  # Unlike #to_s, does not render settings.
  # Will return options, too, since for example, a converter can add
  # label with micro or milli to the options.
  def plots(_options = {})
    options = @options.merge _options
    reduced = @converters.inject(@parys) { |pary, c| c.call pary, options }
    str = do_plots reduced, options
    [str, options]
  end

  def to_s(_options = {})
    str, options = plots _options
    settings.to_s(options) + str
  end

  def to_png(file, options = {})
    # "png" terminal does not support dashed lines.
    to_s options.merge output: "'#{file}'", terminal: 'pngcairo'
  end

  def self.new_pary
    Points.new
  end

  protected

  def plot_line(pary, options, number)
    ary = [
      "using 1:($2 * %d) with %s title '%s'" % [
        options[:multiply] || 1,
        pary.style.to_s(type: options[:type]),
        pary.title || number
      ]
    ] + pary.plotline
    ary.join ' '
  end

  def do_plots(parys, options)
    plot_s = (0..parys.size - 1).map do |i|
      "'-' " + plot_line(parys[i], options, i)
    end.join ', '
    inner = parys.map(&:to_s).join "\ne\n"
    <<EOT


plot #{plot_s}
#{inner}
e
EOT
  end

  class Points < Array
    class Style < Struct.new(:dt, :ps, :lw, :lt, :lc)
      attr_accessor :type

      include SetColor

      # set :lines
      # set :lines, dt: 2
      # set dt: 2
      def set(type = nil, **kw)
        self.type = type if type
        kw.each { |k, v| send "#{k}=", v }
      end

      def to_s(options = {})
        # TODO: If options[:type] is set, do not include non type
        # members.
        ary = type_members.map do |k|
          v = send(k)
          [k, v] if v
        end.compact
        ([options[:type] || type] + ary).join ' '
      end

      def method_missing(k, *args, **kw)
        if [:lines, :points, :linespoints].include? k
          set k, **kw
        else
          super
        end
      end

      protected

      def type_members
        if type == :lines
          members - [:ps]
        elsif type == :points
          members - [:lw, :lt]
        else
          members
        end
      end
    end

    attr_accessor :title, :style, :plotline

    def initialize
      @style = Style.new
      @style.set :lines
      @plotline = []
    end

    def empty
      obj = self.class.new
      [:title, :style, :plotline].each do |m|
        obj.send "#{m}=", send(m)
      end
      obj
    end

    # obj.copy! :style
    # obj.empty.copy! :style
    # obj.clone.copy! :style
    def copy!(*attrs)
      attrs.each do |m|
        send "#{m}=", send(m).clone
      end
      self
    end

    def style(type = nil, **kw)
      @style.set type, **kw
      @style
    end

    def to_s(options = {})
      # point#to_s do not need to serialize in gnuplot format.
      map { |p| [p.x, p.y].join ' ' }.join "\n"
    end

    def add(x, y)
      self << Point.new(x, y)
    end

  end
end

class Plot::Point < Struct.new(:x, :y)
end

class Plot::Renderer
  # gp can be IO or String, something with <<.
  def initialize(gp, plot)
    @gp = gp
    @plot = plot
  end

  # When called second and so on time, do not write
  # settings unless there is something new in options. This
  # helps with window blinking.
  def render(options = {})
    first = @plot.parys.first
    do_render options if first&.any?
  end

  protected

  def do_render(_options)
    str, options = @plot.plots _options
    unless options == @current_options
      @current_options = options
      @gp << @plot.settings.to_s(options)
    end
    @gp << str
  end
end
