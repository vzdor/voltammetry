module SimpleObservable
  attr_reader :observers

  def simple_observable
    @observers = {}
  end

  # observe :done { }
  # observe :done, :iter, { }
  # observe :done, obj.method(:done)
  # observe :done, obj
  def observe(*keys, &block)
    obj = keys.pop if block.nil?
    keys.each do |key|
      observers[key] ||= []
      observers[key] << (block || obj.method(key))
    end
    block
  end

  def observe_if(yes, *keys, &block)
    observe *keys, &block if yes
  end

  def notify(key, arg = nil)
    # array.each will stop if element is deleted from the array
    # while iterating, make a copy.
    ary = observers[key].dup || []
    ary.each { |callable| callable.call arg }
  end
end
