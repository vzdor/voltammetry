require_relative 'plot'

# Spice print format.
#  ngspice -> print v(1) v(2) > v1v2-1-0.spice
class SpiceFile
  attr_reader :text

  def initialize(text)
    @text = text
  end

  def sections
    # TODO: I do not know if "*" indicate beginning or
    # it is legend text.
    s = text.split /^\s+\*/m
    s[1..]
  end

  def parys(**kv)
    sections.inject([]) do |ary, s|
      ary + section_parys(s, **kv)
    end
  end

  def section_parys(section, title: nil)
    # Ignore legend.
    lines = section.lines[3..]
    # Titles.
    s = lines.shift
    # 8 bytes Index title, 16 bytes etc titles.
    spice_titles = s[8..].scan /.{1,16}/
    # '@' is differently shown by Gnuplot.
    spice_titles.map! { |s| s.strip.sub '@', '\@' }
    parys = spice_titles[1..].map do |st|
      pary = Plot.new_pary
      pary.title = [st, title].compact.join ' '
      pary
    end
    # Values.
    lines.each do |line|
      next if line =~ /^[-I]/
      ary = line.split
      _, time, *etc = ary.map { |s| Float(s) }
      etc.each_index do |i|
        millis = etc[i] * 1000
        parys[i] << Plot::Point.new(time, millis)
      end
    end
    parys
  end
end
