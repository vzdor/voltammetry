require_relative 'plot'

class MeterPlot < Plot
  def self.new_pary
    MeterPlot::Points.new
  end
end

class MeterPlot::Points < Plot::Points
  attr_accessor :cmd

  class Cmd < Hash
    def options
      self[:options] || {}
    end

    def sw?
      options[:fun] == 1
    end

    def comments
      self[:comments]
    end

    def method_missing(name, *args, **kw)
      options.has_key?(name) ? options[name] : super
    end

    def observed_dt
      *, dt = comments.find { |s, v| s == 'observed dt' }
      dt
    end
  end

  def initialize
    super
    @cmd = Cmd.new
  end

  def empty
    obj = super
    obj.cmd = cmd
    obj
  end
end
