require_relative 'select_converter'

class CutConverter < ParysConverter
  def initialize(dir = :left)
    dirs = [:left, :right]
    raise "cut: '#{dir}'? Cut right or left from the middle?" unless dirs.include? dir
    @dir = dir
  end

  def call!(parys, opts = {})
    parys.each do |pary|
      if left?
        sel = 0...pary.size/2
      else
        sel = pary.size/2..
      end
      pary.slice! sel
    end
  end

  def left?
    @dir == :left
  end
end
