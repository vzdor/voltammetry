require_relative 'select_converter'

class AtConverter < SelectConverter
  def initialize(v, dv)
    super()
    filter = -> (p) { (p.x - v).abs < dv }
    add_map { |pary| pary.filter! &filter }
  end
end
