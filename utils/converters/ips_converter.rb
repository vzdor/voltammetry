require_relative '../plot'
require_relative 'select_converter'

class IpsConverter < SelectConverter
  def initialize(options = {})
    super()
    @only = options[:on] || (0..)
    add_map do |pary|
      if pary.cmd.sw?
        sw pary
      else
        show pary
      end
    end
  end

  protected

  # The acquire delay between ips and p.y can be noticeable,
  # ignore p.y.

  def show(pary)
    pary.map! do |p|
      is = pick(p.ips).map &:i
      is.map { |i| Plot::Point.new(p.x, i) }
    end.flatten!
  end

  # There can be two or more redox processes going. One of the
  # processes might be unwanted and slower. To compare,
  # view plots with different --ips-only i.
  #
  # If --ips-only m..n, will average the ips from m to n.
  def sw(pary)
    pary.map! do |p|
      is = p.ips.map &:i
      num = is.size/2
      # Split ips into forward and backward pulse arrays.
      fwd, back = *is.each_slice(num)
      # Compute forward - backward diffs.
      ids = *(0..num - 1)
      dis = pick(ids).map { |n| fwd[n] - back[n] }
      # Plot::Point.new p.x, median(dis)
      Plot::Point.new p.x, dis.sum/dis.size
    end
  end

  def median(ary)
    sorted_ary = ary.sort
    sorted_ary[ary.size/2]
  end

  def pick(ary)
    if @only.kind_of? Array
      ary.values_at *@only
    else
      ary[@only]
    end
  end
end
