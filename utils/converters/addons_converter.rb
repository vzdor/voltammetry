require_relative 'select_converter'

class AddonsConverter < ParysConverter
  def call!(parys, options)
    options[:commands] ||= []
    x2y1 parys, options if options.has_key? :x2y1
    parys
  end

  def x2y1(parys, options)
    commands = options[:commands]
    commands << :x2tics
    x2y1 = options[:x2y1]
    parys.each do |pary|
      pary.plotline << 'axes x2y1' if x2y1.include?(pary.title)
    end
  end
end
