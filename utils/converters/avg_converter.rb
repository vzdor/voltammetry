require_relative 'select_converter'
require_relative '../firrol'

class AvgConverter < ParysConverter
  def initialize(win = 1)
    @win = win
  end

  def call!(parys, options = {})
    parys.each &Firrol[:rolling, :median, @win]
    parys
  end
end
