class ParysConverter
  # converters << Converter.new
  # converters << proc { |parys| ... }
  def call(parys, options = {})
    copy = parys.map &:clone
    call! copy, options
    # With number of points from cycle command, copy time is
    # not noticeable:
    # call! parys, options
  end
end

class ProcConverter < ParysConverter
  attr_reader :block

  def initialize(&block)
    @block = block
  end

  def call!(parys, options = {})
    @block.call parys, options
  end
end

class SelectConverter < ParysConverter
  include Enumerable

  attr_reader :converters, :sel

  def initialize(sel = nil, &block)
    @sel = sel || block
    @converters = []
  end

  def call(parys, options = {})
    selected, tail = split parys
    new = @converters.inject(selected) { |arys, c| c.call arys }
    tail + new
  end

  def add_self(obj, *args)
    add obj, *args
    self
  end

  # obj: converter || str || symbol.
  def add(obj, *args, &block)
    converter =
      if obj.kind_of? ParysConverter
        obj
      else
        klass = Kernel.const_get obj.to_s.capitalize + 'Converter'
        klass.new *args, &block
      end
    @converters << converter
    converter
  end

  def add_proc(&block)
    converter = ProcConverter.new &block
    @converters << converter
    converter
  end

  # add_filter { |pary| pary.title == 'x' }
  def add_filter(&block)
    add_proc { |parys, options| parys.filter &block }
  end

  def add_map(&block)
    add_proc { |parys, options| parys.map &block }
  end

  def each(&block)
    @converters.each &block
  end

  protected

  def split(parys)
    if @sel
      _split parys
    else
      [parys, []]
    end
  end

  def _split(parys)
    selp = if @sel.kind_of? Proc
             @sel
           else
             proc { |pary| @sel.include? pary.title }
           end
    parys.partition &selp
  end
end
