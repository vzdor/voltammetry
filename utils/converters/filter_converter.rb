require_relative 'select_converter'
require_relative '../ft-filter/dft'
require_relative '../firrol'

class FilterConverter < ParysConverter
  def initialize(options = {})
    @cutoff = options[:cutoff] || 30
  end

  def dft_filter(pary, n = 6)
    # TODO: Apply (v1 - v2).abs correction to n.
    fys = DFT.forward pary.map &:y
    (n..fys.size - 1 - n).each { |m| fys[m] = 0 }
    iys = DFT.inverse fys
    (0..iys.size - 1).each do |m|
      x = pary[m].x
      y = iys[m].real
      pary[m] = Plot::Point.new x, y
    end
  end

  # True if current distance between any two points is less than 1 uA.
  def micro?(pary)
    min, max = pary.minmax_by &:y
    (min.y - max.y).abs < 0.001
  end

  def sw_filter(pary)
    rol = Firrol::Rolling.new pary
    rol.outliers2
    if micro? pary
      dft_filter pary
      $stdout.puts "filter: rounding to 0.1 micro amp."
      # To look like simple moving median filter and device is not
      # capable of < 0.1 micro amp resolution.
      pary.map! { |p| Plot::Point.new p.x, p.y.round(4) }
    else
      fir = Firrol::Fir.new pary
      fir.lowpass4 *fir4_coefs(pary)
    end
    pary
  end

  # SC goes from some voltage to v1. Then from v1 to v2. Find
  # index of v1, this will be the split index.
  # i(v1) will not necessary coincide with i = size/2.
  def split_index(pary)
    # pary.cmd v1 and v2 - command line values, they mean:
    # iterate counter electrode from gnd + v1 to gnd + v2.
    # While point x = gnd - (gnd + v1) = -v1.
    v = -pary.cmd.v1
    # It is likely device stepped futher.
    if pary.cmd.v1 > pary.cmd.v2
      v -= 100
    else
      v += 100
    end
    dx = proc { |i| (pary[i].x - v).abs }
    pary.each_index.min_by &dx
  end

  def fir4_coefs(pary)
    coefs_db = {
      # key is cutoff frequency when sample frequency is 100.
      # 29 is OK for aug-6-3 --combine 1
      30 => [-0.091862, 0.297272],
      29 => [-0.0734933, 0.295522],
      27 => [-0.036231, 0.289078],
      24 => [0.0172666, 0.274988],
      20 => [0.0784438, 0.25385],
      10 => [0.172609, 0.213356],
      8 => [0.182716, 0.208507],
      2 => [0.198946, 0.200527],
      0 => [0.198946, 0.200527]
    }
    cmd = pary.cmd
    f0 = 10 # dt0/dv0
    f1 = cmd.dv * 1.0/cmd.observed_dt
    i = (f0 * f1 * 29).floor
    i = [i, @cutoff].min
    i -= 1 until coefs_db.has_key? i
    coefs_db[i]
  end

  def sc_filter(pary)
    # End points may look like outliers for outliers algorithm.
    # Split array in two, apply `outliers`. Similarly for filters.
    coefs = fir4_coefs pary
    filter = -> (ary) do
      fir = Firrol::Fir.new ary
      fir.lowpass4 *coefs
    end
    i = split_index pary
    ary1, ary2 = pary[0..i], pary[i + 1..-1]
    filter.call ary1
    # fir#filtfilt is not "symmetric", will not modify
    # ary[-1].y. When reversing, array 1, 2 ends filter similarly.
    ary2.reverse!
    filter.call ary2
    ary2.reverse!
    pary.replace ary1 + ary2
  end

  def filter(pary)
    cmd = pary.cmd
    unless cmd.has_key? :options
      raise 'filter: no cmd, the origin of points is not known.'
    end
    if cmd.sw?
      sw_filter pary
    else
      sc_filter pary
    end
  end

  def call!(parys, options = {})
    parys.each &method(:filter)
  end
end
