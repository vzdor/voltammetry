class UnitsConverter
  PREFIXES = %w(m u)

  def call(parys, options = {})
    p0 = parys.find do |pary|
      pary.find { |p| p.y.abs > 0.1 }
    end
    multiply = p0 ? 1 : 1000
    s = PREFIXES[multiply/1000] + options[:units]
    options[:ylabel] = s
    options[:multiply] = multiply
    parys
  end
end
