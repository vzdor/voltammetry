require 'logger'

# Second communication layer, the first layer is @port which should
# provide #gets, #write and #read_timeout, #read_timeout= methods.

class SimpleCom
  attr_reader :meta0
  attr_reader :logger

  def initialize(port, formatter:, __meta0: nil)
    @port = port
    @formatter = formatter
    @logger = Logger.new 'com.log'
    @meta0 = __meta0 || get_meta0
  end

  def endl
    @port.write "\n"
  end

  def command(cmd, *args)
    line = cmd.dup
    if args.any?
      tmp = serialize_command_args(args).join ' '
      line << ' ' + tmp
    end
    @logger.info "Com#command: #{line}"
    @port.write line
    endl
  end

  def stop
    endl
  end

  def get
    line = @port.gets
    @logger.info "Com#get: #{line&.chomp}"
    @formatter.from_line line
  end

  def until_done
    ary = []
    until (obj = get).done?
      ary << obj
    end
    ary
  end

  def stop_done
    stop
    until_done
  end

  # For services and stubbing in tests.
  public :sleep

  protected

  def serialize_command_args(args)
    args.map &:to_i
  end

  def get_meta0
    init0
    command 'i'
    until_done
  end

  # Tell device to stop the command. If device is initializing,
  # wait until initialized.
  def init0
    # Handle Optiboot bootloader prompt on reset.
    # Sometimes DTR line is connected to controller's RESET pin.
    # @port: the serialport library disable HUPCL (DTR low on
    # process close), but it seems it only takes effect on later
    # open calls.
    # Set timeout time for faster initialization. No need
    # to wait for the bootloader to timeout, Optiboot bootloader
    # will exit when unknown command code is received.
    timeout = @port.read_timeout
    @port.read_timeout = 300
    ok = false
    until ok do
      # Send unique command to make sure the line is last line
      # in the buffer.
      # 2..9 codes not used by the bootloader. stk500.h.
      key = rand(2..9).to_s
      command key
      until ok || (reply = get).name == 'timeout'
        ok = reply.name == 'err' && reply.text.end_with?("#{key}'")
      end
    end
    @port.read_timeout = timeout
  end
end
