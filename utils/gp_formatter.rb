# Gnuplot compatible format, which means the file can be
# plotted from gnuplot console. Will not lose ips and save them
# in gnuplot comment format.
class GpFormatter
  class << self
    def dump(pary)
      serialized = pary.map do |obj|
        *, name = obj.class.name.split '::'
        send "serialize_#{name.downcase}", obj
      end
      serialized.join "\n"
    end

    def serialize_values(point)
      point.ips.map { |ip| "# ip #{ip.i}" } + [serialize_point(point)]
    end

    def serialize_point(point)
      "#{point.x} #{point.y}"
    end

    def from_line(line)
      if line[0..3] == '# ip'
        i = line[5..].to_f
        Prot::Ip.new i
      else
        ref, amp, = line.split(' ').map &:to_f
        Prot::Values.new ref, amp
      end
    end

    def from_text(pary, str)
      reader = Prot::ValuesReader.new
      enum = str.lines.to_enum
      pary << reader.next { from_line enum.next } while enum.peek
    rescue StopIteration
    end
  end
end
