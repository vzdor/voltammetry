if RUBY_PLATFORM =~ /linux/
  require_relative 'devices'
else
  require_relative 'dummy_devices'
  Devices = DummyDevices
end

require_relative 'defp'

module Arguments
  class << self
    extend Defp

    defp :device, dir: Devices

    defp :device, :s, where: {s: nil} do
      devs = dir.find_all
      if devs.empty?
        raise 'No devices found.'
      elsif devs.size > 1
        raise devs.join(', ') + '. Select device with --dev argument.'
      end
      devs.first
    end

    # Called when s is not nil.
    defp :device do |s|
      raise "'#{s}' device not found." unless dir.exist? s
      s
    end

    defp :bool, v: false do |s = v|
      s == true || s == 'yes'
    end
    # ^ is equal to:
    # def bool(v = false)
    #   -> (s = v) { s == true || s == 'yes' }
    # end

    defp :sym, &:to_sym
    # ^ is similar, but without default value, v:
    # def sym(v = nil)
    #   -> (s = v) { s.to_sym }
    # end

    def int(v = nil)
      -> (s = v) { s.to_i }
    end

    def dc(options = {})
      {
        v: 0,
        delay: 0,
        steps: 3,
      }.merge(options)
    end

    def motor
      {
        on: bool,
        dc: 15
      }
    end

    # optional
    # optional { |s| s }
    # optional :device
    defp :optional, sym: nil

    defp :optional do |s|
      c = sym ? Arguments.send(sym) : block
      if s.nil?
      elsif c
        c.call s
      else
        s
      end
    end

    # nil -> 0..
    defp :selector, :s, where: {s: nil} do
      0..
    end

    # '1..' -> 1..
    defp :selector, :s, where: {
           s: -> (s) { s.include? '..' }
         } do |s|
      b, c = s.split('..').map &:to_i
      b..c
    end

    # '1, 2, 3' -> [1, 2, 3]
    defp :selector do |s|
      s.split(',').map &:to_i
    end
    # ^ is equal to:
    # defp :selector do |s = '0..'|
    #   if s.include? '..'
    #     b, c = s.split('..').map &:to_i
    #     b..c
    #   else
    #     s.split(',').map &:to_i
    #   end
    # end

    def ampfu(options = {})
      {
        fu: 0,
        cmd: optional { |s| s.split ',' }
      }.merge(options)
    end

    def defaults(options = {}, &block)
      _defaults =
        {
          dev: device,
          dir: 'tmp',
          diff: bool,
          quiet: bool,
          gnd: 1/2r,
          motor: motor,
          plot: {
            xlabel: optional,
            ylabel: optional,
            # xtics: 'rotate 0.1',
            xtics: optional,
            # ytics: 'autofreq',
            ytics: optional,
            font: optional,
            # ylabel will be set to {u, m}units.
            units: 'A',
            # Set points array axes to x2y1 if title is in x2y1.
            x2y1: optional { |s| s.split ',' },
            # "key opaque,x2label 'V'" will be sent to gnuplot as:
            #  set key opaque
            #  set x2label 'V'
            commands: optional { |s| s.split ',' },
            # lines, points, linespoints.
            type: optional(:sym),
          },
          fc: 3,
          # WE amps acquire function and options.
          ampfu: ampfu,
          mils: bool
        }
      _options = block_given? ? instance_eval(&block) : options
      _defaults.merge _options
    end
  end
end
