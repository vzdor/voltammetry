require_relative 'prot'
require_relative 'simple_observable'
require_relative 'simple_com'

class Com < SimpleCom
  def initialize(port, formatter: Prot::Formatter, __meta0: nil)
    super
    @port.read_timeout = 5000
    @gnd = @meta0[-1].gnd
  end

  def dc(v)
    command 'v', v + gnd
  end

  # meta0[1].gnd can differ from @gnd due to TIA limited ability
  # to handle current.
  # On the other hand, #gnd is used to calculate potentials
  # in cycle and dc methods and should not depend on temporary
  # variations.
  # So, return @gnd.
  def gnd
    @gnd
  end

  def gnd=(f)
    @gnd = f.kind_of?(Hash) ? f[:v] : (f * vcc).floor
    command 'g', @gnd
    stop_done
  end

  # Supply voltage in millivolts.
  def vcc
    md = meta0[0].text.match /vcc (\d+)/
    md.captures[0].to_i
  end

  def motor(options)
    command 'p', options[:on] ? 1 : 0, options[:dc]
    until_done
  end

  def cycle(options)
    # Ruby 3.
    # options => {fun:, dv:, delay:, count:, v1:, v2:}
    fun, dv, delay, count, v1, v2 = options.values_at :fun, :dv, :delay, :count, :v1, :v2
    add = [0]
    if fun == 1
      add[0] = options[:ep]
    end
    command 'c', fun, dv, delay, count, gnd + v1, gnd + v2, *add
  end

  def values
    command 'i'
  end

  def mils=(on)
    self.ampfu = [2, on ? 1 : 0]
  end

  def fc=(n)
    self.ampfu = [1, n]
  end

  def ampfu=(values)
    command 'f', *values
    until_done
  end

  def comment(name, text)
    meta0 << Prot::Comment.new(name, text)
  end
end
