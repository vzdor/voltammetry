## On filter cutoff and voltammogram dv/dt

With lower concentration, the peak will be lower and look wider, lower frequency filter can be used. Below I assumed only dv or dt changes and peak stay the same, but it is not.




Suppose filter fc0/fs0, where fs0 is sample frequency and fc0 is cutoff frequency, is designed for dv0/dt0 voltammogram. If this filter applied to different dv1/dt1 voltammogram:

1. `dt1 = dt0`, `dv1 = 2 dv0`. This means 2 times less samples per Volt, but the voltammogram signal looks the same. For the filter, the signal will look 2 times the frequency. This can be expressed as lower cutoff frequency: `fc' = 1/2 fc0`
2. Same number of samples, `dv1 = dv0`, but `dt1 = 2 dt0`. 1/dt is sampling frequency and it is twice lower. `fs' = 1/2 fs0`
3. If `dv1 = 2 dv0` and `dt1 = 2 dt0`, `fc'/fs' = fc0/fs0`.

```
fc'   dt1 dv0 fc0     fc0
--- = ----------- = g ---
fs'   dt0 dv1 fs9     fs0
```

### To select filter given fc0/fs0 and g

If dv1 = 2 dv0,  g = 1/2. fc'/fs' will be lower, 1/2 fc0/fs0. Selecting new fc1/fs1 for fc0/fs0: `fc1/fs1 = g^-1 fc0/fs0`.

## DFT

Discrete Fourier transform (DFT) plot point is given by: `(xi = 1/dt / samples i, yi)`. The first term, xi, is frequency. i is index in array of points. 1/dt is sample frequency. `samples = v/dv`, v is abs(v1 - v2), so `xi = dv/dt i/v`. If v = 1 Volt and i = 1, `x1 = dv/dt / (1 Volt)` will be first frequency on DFT plot.

DFT can be used as a filter. If `y_{i > n}` set to 0, the inverse transform will be signal with `x_{i > n}` frequencies removed.

### DFT filter is dv/dt self-adjusting

Lets compare `g^-1` and `i dv/dt`.

```
(%i2) ev(dv * i/dt, dv = dv0 * 2, dt = dt0);
                                    2 dv0 i
(%o2)                               -------
                                      dt0

(%i3) ev(dv * i/dt, dt = dt0 * 2, dv = dv0);
                                     dv0 i
(%o3)                                -----
                                     2 dt0
```

```
(%i4) ev(g^-1, dv1 = 2 * dv0, dt1 = dt0);
(%o4)                                  2
(%i5) ev(g^-1, dv1 = dv0, dt1 = 2 * dt0);
                                       1
(%o5)                                  -
                                       2
```

If DFT filter that adjusts frequencies `x_{i > n}` is OK for dv0/dt0 voltammogram, the filter will be OK for dv1/dt1. There is no need in table of filters.
