# Perform DFT transforms, forward and inverse.
# @author James Tunnell

# * I deleted even samples requirement. From [1], for even number
#   of samples: f at N/2 is fs/2. For odd number of samples:
#   f at N/2 is not defined. OK for me.
# * code style edits.

# 1. https://mil.ufl.edu/nechyba/www/__eel3135.s2003/lectures/lecture19/dft.pdf

class DFT
  TWO_PI = Math::PI * 2

  # @param [Array] input  array of real values, representing the
  # time domain signal to be passed into the forward DFT.
  def self.forward(input)
    output = Array.new input.size
    output.each_index do |k|
      sum = Complex 0.0
      input.each_index do |n|
        a = TWO_PI * n * k / input.size
        sum += Complex(input[n] * Math::cos(a), -input[n] * Math::sin(a))
      end
      output[k] = sum
    end
    output
  end

  # @param [Array] input  array of complex values, representing
  # the frequency domain signal obtained from the forward DFT.
  def self.inverse(input)
    output = Array.new input.size
    output.each_index do |k|
      sum = Complex 0.0
      input.each_index do |n|
        a = TWO_PI * n * k / input.size
        sum += Complex(input[n] * Math::cos(a), input[n] * Math::sin(a))
      end
      output[k] = sum / output.size
    end
    output
  end
end
