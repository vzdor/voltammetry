require_relative '../converters/select_converter'
require_relative 'dft'

class DftConverter < ParysConverter
  def initialize(dt)
    @dt = dt
  end

  def select_dt(pary)
    if @dt
      @dt
    elsif pary.cmd.has_key? :comments
      pary.cmd.observed_dt
    else
      raise 'dt not found in pary, but is required to determine sample frequency (1/dt).'
    end/1000.0 # dt is in ms.
  end

  # Find frequency spectrum.
  def fs(pary)
    ys = pary.map &:y
    # Add zeros for denser frequency points.
    (ys.size * 4).times { ys << 0 }
    fys = DFT.forward ys
    dt = select_dt pary
    (0..fys.size/2).map do |i|
      y = fys[i].abs
      pary[i] = Plot::Point.new (1/dt / fys.size) * i, y
    end
  end

  def call!(parys, options = {})
    parys.each &method(:fs)
  end
end
