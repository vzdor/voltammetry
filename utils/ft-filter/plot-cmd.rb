require_relative '../experiment'
require_relative '../arguments'
require_relative '../pary_files'
require_relative '../firrol'

require_relative 'dft_converter'

scheme = Arguments.defaults do
  {
    dev: optional(:device),
    combine: selector,
    dt: optional(:int),
    outliers: bool
  }
end

experiment(scheme, title: 'DFT.') do
  s = SelectConverter.new
  s.add_map &Firrol[:rolling, :outliers2] if options[:outliers]
  s.add :dft, options[:dt]
  plot.converters << s
  pf = ParyFiles.new dumpster
  plot.parys.concat pf.all(options[:combine], options)
  gp.puts plot.to_s
end
