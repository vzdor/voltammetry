require_relative 'prot'
require_relative 'meter_plot'
require_relative 'spice_file'
require_relative 'gp_formatter'

require 'json'

# The thing knows how to look for files in the store and who knows the
# format of the file, but does not know the format.

class ParyFiles
  attr_reader :dumpster

  def initialize(dumpster)
    @dumpster = dumpster
  end

  # TODO: obj: 1..5,7..9,22.. instead of --combine-omit.

  # obj: [1, 2, 3]
  # obj: (1..3)
  # obj: {on: (1..), omit: [2]}
  def all(obj, options = {})
    sel = obj
    omit = []
    if obj.kind_of? Hash
      sel = obj[:on]
      omit = obj[:omit] if obj.has_key? :omit
    end
    if sel.include? Float::INFINITY
      i = dumpster.iter
      sel = (sel.begin..i)
    end
    sel.map do |n|
      next [] if omit.include? n
      _parys = parys n, options
      yield _parys if block_given? && _parys.any?
      _parys
    end.flatten 1
  end

  # n: 1, 2, ...
  # i-n-0.spice
  # v-n-1.spice
  # n-0.pary
  # n-1.pary
  def parys(n, _options = {})
    options = {iter: 0..}.merge _options
    pattern = "{v-,i-,}#{n}-*.{pary,spice}"
    files = dumpster.glob(pattern).sort
    sel = options[:iter]
    sel_files = files.slice(sel) || []
    ary = sel_files.map { |file| from file }
    ary.flatten 1
  end

  def from(file)
    title = file.sub /.(spice|pary)/, '.'
    if file.end_with? '.spice'
      sf = SpiceFile.new dumpster.read(file)
      sf.parys title: title
    else
      pary = MeterPlot.new_pary
      pary.title = title
      GpFormatter.from_text pary, dumpster.read(file)
      cmd_file = file + '-cmd'
      fill_cmd pary, cmd_file if dumpster.exist? cmd_file
      [pary]
    end
  end

  protected

  def fill_cmd(pary, file)
    s = dumpster.read file
    cmd = JSON.parse s, symbolize_names: true
    pary.cmd.merge! cmd
  end
end
