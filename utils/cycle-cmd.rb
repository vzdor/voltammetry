require_relative 'experiment'
require_relative 'arguments'

scheme = Arguments.defaults do
  {
    dc: dc(v: 870, delay: 5, steps: 5),
    cycle: {
      fun: 0,
      dv: 30,
      delay: 100,
      count: 2,
      v1: 300,
      v2: 870,
      ep: 50
    },
    idle: bool,
    view: bool(true)
  }
end

experiment(scheme, title: 'Cycle.') do
  dc options[:dc]
  c.stop_done

  unless options[:idle]
    cycle :add do
      gp.puts plot.to_s(terminal: 'qt noraise') if options[:view]
    end
    tell comments
    save
    bell
  end
end
