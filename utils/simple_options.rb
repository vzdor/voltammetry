def simple_options(options, argv = ARGV)
  i = 0
  while i < argv.size
    k = argv[i]
    raise "`#{k}` does not look like `--key [value]`." unless k =~ /^--/
    k_i = i
    # Set next key index.
    i += 1
    v = argv[i]
    # --motor --x y => --motor yes --x y
    if v =~ /^--/
      v = nil
    else
      # It was key value, set next key index.
      i += 1
    end

    # Allows first option priority:
    #  cat options.txt | xargs cmd.rb --delay 10
    #  `--delay 10` will have priority over the same option in options.txt.
    if argv.index(k) < k_i
      kv = [k, v].compact.join ' '
      $stderr.puts "`#{kv}` is ignored, already set."
      next
    end

    syms = k.split('-').reject(&:empty?).map(&:to_sym)

    # Find element in the scheme.
    ele = options
    _options = sym = nil
    while syms.any? && ele
      sym = syms.shift
      _options = ele
      ele = ele[sym]
    end
    raise "`#{k}` is not found in scheme." unless ele

    # --motor [yes] => --motor-on yes, for familiar
    #  command line notation.
    if ele.kind_of?(Hash) && ele[:on].respond_to?(:call)
      _options = ele
      sym = :on
      ele = ele[sym]
    end

    if v.nil?
      unless ele.respond_to? :call
        raise "`#{k}` should have value, `#{k} #{ele.class}`."
      end
      v = 'yes'
    end

    new = if [Integer, Float, Rational].include? ele.class
            Kernel.send ele.class.to_s, v
          elsif ele.kind_of? Symbol
            v.to_sym
          elsif ele.respond_to? :call
            ele.call v
          else
            v
          end
    _options[sym] = new
  end
  # Recursively call procs for which there is no argument. So,
  #  simple_options({x: optional(:bool)}, []) => {}
  #  simple_options({x: gnd(1/3r)}, []) => {gnd: 1/3r}.
  options.each do |k, v|
    case v
    when Method, Proc
      # Delete key if value is nil, for options.has_key?(k).
      val = v.call
      unless val.nil?
        options[k] = val
      else
        options.delete k
      end
    when Hash
      simple_options v, []
    end
  end
  options
end
