require_relative 'experiment'
require_relative 'arguments'

scheme = Arguments.defaults do
  {
    gnd: 1/3r,
    ampfu: ampfu(fu: 11, samples: 16, ips: 4),
    idle: {
      on: optional(:bool),
      dc: dc(v: 300, steps: 5),
    },
    dc: dc(v: 870, delay: 300, steps: 10, mf: {i: 30, max: 50, dc: 16, step: 2}),
    stopdelay: 3000,
    cycle: {
      on: optional(:bool),
      fun: 1,
      dv: 10,
      delay: 40,
      count: 0,
      v1: 300,
      v2: 0,
      ep: 50
    },
    jump: {
      on: optional(:bool),
      dc: dc(v: 870, steps: 0, delay: 5),
    }
  }
end

experiment(scheme, title: 'Syringe holder copper electrodes Pb detector.') do
  def jump
    dc options[:jump][:dc]
    c.stop_done
  end

  if options[:idle].has_key? :on

  elsif options[:cycle].has_key? :on
    cycle :add
  else
    dc
    c.stop_done
    jump if options[:jump].has_key? :on
    c.motor on: false
    sleep options[:stopdelay]/1000.0
    cycle :add
  end

  dc options[:idle][:dc]
  c.stop_done

  if plot.parys.any?
    save
    tell comments
    bell
  end
end
