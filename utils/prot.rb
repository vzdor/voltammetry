# coding: utf-8

module Prot
end

class Prot::Message
  attr_accessor :name

  def done?
    name == 'done'
  end
end

class Prot::Comment < Prot::Message
  attr_reader :text

  def initialize(name, text = nil)
    @name = name
    @text = text
  end

  def to_s
    ['comment', name, text].compact.join ' '
  end
end

# This can be plotted and have some fields, that is all. It is the
# role of Formatter to correctly instantiate it.
class Prot::Values < Prot::Message
  attr_accessor :ref
  attr_accessor :amp
  attr_accessor :ips
  attr_accessor :ce
  attr_accessor :gnd

  # ce is necessary for Com, but not always.
  def initialize(ref, amp, ce: nil, gnd: 0)
    @ref = ref
    @amp = amp
    @gnd = gnd
    @ce = ce
    @ips = []
    @name = 'values'
  end

  # What to plot?
  alias :x :ref
  alias :y :amp

  # Format for humans.
  def to_s
    ['values', ref, amp, ce, gnd].join ' '
  end
end

class Prot::Ip < Prot::Comment
  attr_reader :i

  def initialize(i)
    @i = i
    super 'ip', i.to_s
  end
end

# Com knows types but does not know their format besides that they
# came from lines it read from Com#port.

# Formatter encapsulates:
#  line format
#  instantiation of types from lines.

class Prot::Formatter
  def initialize(line)
    # Workaround: "abcあ\x81" =~ /\d/
    # err: Invalid byte sequence in UTF-8.
    @line = ascii line if line
  end

  def obj
    if @line.nil?
      Prot::Comment.new 'timeout'
    # 4'th element is current and it can be negative.
    elsif md = @line.match(/^(\d+) (\d+) (\d+) (-?\d+)\z/)
      new_values *(md.captures.map &:to_i)
    elsif md = @line.match(/^# (\w+) ?(.*)?/)
      new_ip_or_comment *md.captures
    else
      new_line
    end
  end

  class << self
    def from_line(line)
      new(line).obj
    end
  end

  protected

  def milliamps(i)
    i/10000.0
  end

  def new_ip_or_comment(name, text)
    if name == 'ip'
      Prot::Ip.new milliamps(text.to_i)
    else
      Prot::Comment.new name, text
    end
  end

  def new_values(ce, gnd, ref, amp)
    amp = milliamps amp
    ref -= gnd
    ce -= gnd
    # WE is at gnd.
    # -ref: -(ref - WE) = WE - ref or WE relative to ref.
    # - sign indicate WE is lower than ref.
    Prot::Values.new -ref, amp, ce: ce, gnd: gnd
  end

  def new_line
    Prot::Comment.new 'err', 'Unknown format: ' + @line
  end

  def ascii(s)
    s.bytes.filter { |b| (32...127).include? b }.pack 'c*'
  end
end

# Encapsulates values with ips protocol.
class Prot::ValuesReader
  # ip
  # ip
  # vals
  # iter||done||ip
  def next(&block)
    ips = []
    while (obj = block.call).name == 'ip'
      ips << obj
    end
    # Can be iter or done, in this case ips is blank.
    obj.ips = ips if obj.name == 'values'
    obj
  end
end
