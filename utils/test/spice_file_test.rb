require "minitest/autorun"

require_relative '../spice_file'

describe SpiceFile do
  let(:text2) { File.read 'test/fixtures/v1v2-2-0.spice' }

  it 'returns parys' do
    sp = SpiceFile.new text2
    parys = sp.parys
    assert_equal 2, parys.size
    ary1, ary2 = parys
    assert_equal 'v(1)', ary1.title
    assert_equal 'v(2)', ary2.title
    assert_equal 2, ary1.size
    # v(1)
    assert_equal [0, 1e-7], ary1.map(&:x)
    assert_equal [300, 299.5], ary1.map(&:y)
    # v(2)
    assert_equal [0, 1e-7], ary2.map(&:x)
    assert_equal [300, 300], ary2.map(&:y)
  end

  it 'adds file identity to title' do
    sp = SpiceFile.new text2
    ary1, = sp.parys title: '1-1.'
    assert_equal ary1.title, 'v(1) 1-1.'
  end

  describe 'titles with spaces and @' do
    it 'adds titles with @ replaced by \@' do
      text = File.read 'test/fixtures/i-3-0.spice'
      sp = SpiceFile.new text
      titles = sp.parys.map &:title
      assert_equal ['i(v1) + \@r2[i]', '\@c1[i]', 'i(v1)', '\@r2[i]'], titles
    end
  end
end
