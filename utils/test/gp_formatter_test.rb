require "minitest/autorun"

require_relative '../plot'
require_relative '../prot'
require_relative '../gp_formatter'

describe GpFormatter do
  let(:vals) { Prot::Values.new 1, 2 }

  describe '::dump' do
    it 'serializes values in gnuplot format' do
      str = GpFormatter.dump [vals, vals]
      assert_equal "1 2\n1 2", str
    end

    describe 'with ips' do
      before do
        vals.ips << Prot::Ip.new(1)
        vals.ips << Prot::Ip.new(2)
      end

      it 'serializes in gp format' do
        str = GpFormatter.dump [vals]
        fmt =<<EOT.chomp
# ip 1
# ip 2
1 2
EOT
        assert_equal fmt, str
      end
    end

    it 'serializes plot points in gp format' do
      point = Plot::Point.new 1, 2
      str = GpFormatter.dump [point, point]
      assert_equal "1 2\n1 2", str
    end

  end

  describe '::from_text' do
    it 'fills pary' do
      str = <<EOT
# ip 1.1
# ip 2.2
5.1 6.2
EOT
      pary = []
      GpFormatter.from_text pary, str
      assert_equal 1, pary.size
      point, = pary
      assert_equal [5.1, 6.2], [point.x, point.y]
      assert_equal [1.1, 2.2], point.ips.map(&:i)
    end
  end
end
