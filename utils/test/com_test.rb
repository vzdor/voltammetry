# coding: utf-8
require "minitest/autorun"

require_relative '../com'

describe Com do
  let(:meta0) do
    [
      Prot::Comment.new('init', 'vcc 4855'),
      Prot::Values.new(1200, 100, gnd: 1000)
    ]
  end

  let(:port) { Minitest::Mock.new }

  let(:com) { Com.new port, __meta0: meta0 }

  before do
    # Called from constructor.
    port.expect :read_timeout=, nil, [5000]
  end

  describe '#comment' do
    it 'adds comment to meta0' do
      com.comment 'cycle time', 10
      *, comment = com.meta0
      assert_equal 'cycle time', comment.name
    end
  end

  describe '#vcc' do
    it 'returns vcc from # init' do
      assert_equal 4855, com.vcc
    end
  end

  def command_called(args, &block)
    mock = Minitest::Mock.new
    mock.expect :call, nil, args
    # #stub will call: mock.call(args).
    com.stub :command, mock, &block
    mock.verify
  end

  describe '#gnd' do
    it 'returns gnd from meta0' do
      assert_equal 1000, com.gnd
    end

    it 'returns memorized gnd' do
      cmd = proc { com.gnd = {v: 100} }
      com.stub :stop_done, [] do
        command_called ['g', 100], &cmd
      end
      assert_equal 100, com.gnd
    end
  end

  describe '#dc' do
    it 'sends dc command' do
      command_called ['v', 1100] do
        com.dc 100
      end
    end
  end

  describe '#cycle' do
    it 'sends cycle command' do
      command_called ['c', 0, 10, 50, 1, 1000, 2000, 0] do
        com.cycle fun: 0, dv: 10, delay: 50, count: 1, v1: 0, v2: 1000
      end
    end

    describe 'when fun = 1 and ep' do
      it 'sends cycle command with ep' do
        command_called ['c', 1, 10, 50, 1, 1000, 2000, 50] do
          com.cycle fun: 1, dv: 10, delay: 50, count: 1, v1: 0, v2: 1000, ep: 50
        end
      end
    end
  end

  describe '#ampfu=' do
    it 'sends ampfu command' do
      cmd = proc { com.ampfu = [1, 2, 3] }
      com.stub :until_done, [] do
        command_called ['f', 1, 2, 3], &cmd
      end
    end
  end

  describe '#fc=' do
    it 'sends fc command' do
      cmd = proc { com.fc = 1 }
      com.stub :until_done, [] do
        command_called ['f', 1, 1], &cmd
      end
    end
  end
end
