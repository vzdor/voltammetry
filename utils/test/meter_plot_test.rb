require "minitest/autorun"

require_relative '../meter_plot'

describe MeterPlot::Points::Cmd do
  let(:cmd) { MeterPlot::Points::Cmd.new }

  describe 'observed_dt' do
    it 'returns "observed dt" from comments' do
      cmd[:comments] = [['observed dt', 1]]
      assert_equal 1, cmd.observed_dt
    end
  end

  describe '#sw?' do
    describe 'with blank options' do
      it 'returns false' do
        assert ! cmd.sw?
      end
    end
  end
end
