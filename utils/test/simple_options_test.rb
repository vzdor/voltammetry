require "minitest/autorun"

require_relative '../simple_options'

describe 'simple_options' do
  it 'returns options' do
    opts = simple_options({cycle: {v: 1}}, ['--cycle-v', '2'])
    assert_equal opts[:cycle][:v], 2
  end

  describe 'with unknown argument' do
    it 'raises error' do
      assert_raises do
        simple_options({cycle: {v: 1}}, ['--typo-v', '2'])
      end
      assert_raises do
        simple_options({cycle: {v: 1}}, ['--cycle-typo', '2'])
      end
    end
  end

  describe 'with proc' do
    it 'calls proc and assigns its return value' do
      opts = simple_options({cycle: {v: -> x { x.to_i }}}, ['--cycle-v', '2'])
      assert_equal opts[:cycle][:v], 2
    end

    it 'calls proc to assign default value' do
      opts = simple_options({dev: -> (x = nil) { 1 }}, [])
      assert_equal opts[:dev], 1
    end

    it 'calls recursively to assign default value' do
      opts = simple_options({motor: {on: -> (x = nil) { 1 }}}, [])
      assert_equal 1, opts[:motor][:on]
    end

    it 'deletes keys with nil values' do
      opts = simple_options({z: -> () {}}, [])
      assert ! opts.has_key?(:z)
    end
  end

  describe 'with --key yes while key is Hash' do
    it 'sets key[:on]' do
      scheme = {
        x: {
          z: 1,
          on: -> (x = nil) { x == 'y' }
        }
      }
      opts = simple_options scheme, ['--x', 'y', '--x-z', '5']
      assert_equal 5, opts[:x][:z]
      assert opts[:x][:on]
    end
  end

  describe 'with --key [val], val is omitted' do
    it 'assigns `yes` to value' do
      scheme = {
        x: -> (x) { x },
        y: 1,
        z: -> (x) { x },
      }
      opts = simple_options scheme, ['--x', '--y', '-5', '--z']
      assert_equal 'yes', opts[:x]
      assert_equal -5, opts[:y]
      assert_equal 'yes', opts[:z]
    end

    describe 'with non proc key handler' do
      it 'raises error' do
        scheme = {
          x: 1
        }
        assert_raises do
          opts = simple_options scheme, ['--x']
        end
      end
    end
  end

  describe 'with -- omitted key' do
    it 'raises error' do
      assert_raises do
        simple_options({cycle: {v: 1}}, ['-cycle-v', '2'])
      end
    end
  end

  describe "when value can't be converted" do
    it 'raises error' do
      assert_raises do
        simple_options({x: 1}, ['--x', 'yes'])
      end
    end
  end
end
