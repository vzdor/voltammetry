# coding: utf-8
require "minitest/autorun"

require_relative '../prot'

describe Prot::Formatter do
  it 'returns ip' do
    ip = Prot::Formatter.from_line '# ip 1'
    assert_equal Prot::Ip, ip.class
    assert_equal 0.0001, ip.i
  end

  it 'returns values' do
    vals = Prot::Formatter.from_line '1 2 3 -4'
    assert_equal Prot::Values, vals.class
    assert_equal -4/10000.0, vals.amp
    assert_equal -1, vals.ref
    assert_equal 2, vals.gnd
    assert_equal -1, vals.ce
  end

  it 'returns unknown format comment' do
    obj = Prot::Formatter.from_line '@ 1'
    assert obj.kind_of?(Prot::Comment)
    assert_equal 'err', obj.name
  end

  it 'fixes invalid byte sequence' do
    comment = Prot::Formatter.from_line "# x abcあ\x81"
    assert_equal 'abc', comment.text
  end
end

describe Prot::Values do
  describe '#to_s' do
    it 'returns human format' do
      vals = Prot::Values.new 1, 2, ce: 3, gnd: 4
      assert_equal 'values 1 2 3 4', vals.to_s
    end
  end
end

describe Prot::Comment do
  describe '#to_s' do
    it 'returns human format' do
      comment = Prot::Comment.new 'values num', '1'
      assert_equal 'comment values num 1', comment.to_s
    end
  end
end

describe Prot::ValuesReader do
  describe '#next' do
    it 'returns values with ips' do
      ip1 = Prot::Ip.new 1
      ip2 = Prot::Ip.new 2
      vals = Prot::Values.new 1, 1
      ary = [ip1, ip2, vals]
      reader = Prot::ValuesReader.new
      read = reader.next { ary.shift }
      assert_equal vals, read
      assert_equal [ip1, ip2], read.ips
    end
  end
end
