require "minitest/autorun"

require_relative '../pary_files'

describe ParyFiles do
  describe '#from' do
    let(:pf) { ParyFiles.new File }

    it 'returns pary' do
      pary, = pf.from 'test/fixtures/1-0.pary'
      assert pary
      p1, = pary
      assert_equal -0.82, p1.x
      assert_equal 0.0002, p1.y
    end

    it 'loads ips' do
      pary, = pf.from 'test/fixtures/ips/1-0.pary'
      assert pary
      assert_equal 2, pary.size
      p1, = pary
      assert_equal 5, p1.ips.size
      ip1, = p1.ips
      assert_equal 0.0001, ip1.i
    end

    it 'loads cmd' do
      pary, = pf.from 'test/fixtures/1-0.pary'
      cmd = pary.cmd
      assert cmd[:options]
      assert_equal 10, cmd[:options][:dv]
      observed = cmd[:comments].find { |s,| s == 'observed dt' }
      assert_equal ['observed dt', 150], observed
    end
  end

  describe '#all' do
    let(:dumpster) { Minitest::Mock.new }
    let(:pf) { ParyFiles.new dumpster }

    it 'calls #parys for selected indexes' do
      pf.stub :parys, proc { |n| n } do
        ary = pf.all [1, 2]
        assert_equal [1, 2], ary
      end
    end

    describe 'with :omit' do
      it 'omits index' do
        pf.stub :parys, proc { |n| n } do
          ary = pf.all on: [1, 2, 3], omit: [2]
          assert_equal [1, 3], ary
        end
      end
    end
  end
end
