require "minitest/autorun"

require_relative '../defp'

module T1
  class << self
    extend Defp

    defp :x, s: 1 do |s2|
      [s, s2]
    end
  end
end

module T2
  class << self
    extend Defp

    defp :x, :s, where: {s: nil} do
      1
    end

    defp :x do |s|
      s
    end
  end
end

module T3
  class << self
    extend Defp

    defp :x do |s|
      block.call s
    end
  end
end

module T4
  class << self
    extend Defp

    defp :x, :s, where: {
           s: -> (s) { s > 2 }
         } do |s|
      s * s
    end

    defp :x do |s|
      s
    end
  end
end

describe Defp do
  it 'adds x' do
    assert T1.respond_to? :x
  end

  describe '::x' do
    it 'returns callable' do
      x = T1.x
      assert x.respond_to? :call
    end

    describe 'block' do
      it 'calls block' do
        x = T1.x
        assert_equal [1, 2],  x.call(2)
      end

      it 'sets var' do
        x = T1.x 4
        assert_equal [4, 2], x.call(2)
      end
    end
  end

  describe 'where' do
    it 'filters block' do
      x = T2.x
      assert_equal 1, x.call
      assert_equal 2, x.call(2)
    end

    describe 'proc' do
      it 'filters block' do
        x = T4.x
        assert_equal 2, x.call(2)
        assert_equal 9, x.call(3)
      end
    end
  end

  describe 'given block' do
    it 'calls block' do
      x = T3.x { |s| s }
      assert_equal 1, x.call(1)
    end
  end
end
