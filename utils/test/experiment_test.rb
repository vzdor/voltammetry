require "minitest/autorun"

require_relative '../experiment'

class MemoryDumpster
  attr_reader :dump

  def initialize
    @dump = []
  end

  def save(&block)
    yield 1, @dump
  end

  def fp(file)
    file
  end
end

describe Experiment do
  let(:com) { Minitest::Mock.new }

  let(:dumpster) { MemoryDumpster.new }

  let(:ex) { Experiment.new com, dumpster, quiet: true, plot: {}, gp: StringIO.new }

  let(:pary) { ex.plot.new_pary }

  before do
    com.expect :nil?, false
  end

  describe '#save' do
    before do
      pary.add 1, 0.0001
      com.expect :meta0, ['# observed dt 120']
    end

    it 'saves formatted pary' do
      formatter = {}
      def formatter.dump(pary)
        'ok'
      end
      ex.save formatter: formatter
      obj = dumpster.dump.find { |name,| name == '1-0.pary' }
      assert obj
      name, value = obj
      assert_equal 'ok', value
    end

    it 'saves options as json' do
      ex.save
      obj = dumpster.dump.find { |name,| name == 'options-1.txt' }
      assert obj
      name, value = obj
      t =<<EOT.chomp
{
  "quiet": true,
  "plot": {
  }
}
EOT
      assert_equal t, value
    end

    it 'saves meta0 as json' do
      ex.save
      obj = dumpster.dump.find { |name,| name == 'meta0-1.txt' }
      assert obj
      name, value = obj
      t =<<EOT.chomp
[
  "# observed dt 120"
]
EOT
      assert_equal t, value
    end

    it 'saves pary cmd' do
      pary.cmd = {
        options: {
          dt: 10
        },
        comments: [['observed dt', 100]]
      }
      ex.save
      *, cmd =  dumpster.dump
      name, str = cmd
      assert_equal '1-0.pary-cmd', name
      t =<<EOT.chomp
{
  "options": {
    "dt": 10
  },
  "comments": [
    [
      "observed dt",
      100
    ]
  ]
}
EOT
      assert_equal t, str
    end
  end
end
