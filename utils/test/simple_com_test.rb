require "minitest/autorun"

require_relative '../simple_com'

describe SimpleCom do
  let(:port) { Minitest::Mock.new }

  let(:formatter) { Minitest::Mock.new }

  let(:com) { SimpleCom.new port, formatter: formatter, __meta0: [] }

  describe '#get' do
    it 'returns formatted' do
      port.expect :gets, 'ok'
      formatter.expect :from_line, :ok, ['ok']
      assert_equal :ok, com.get
    end

    describe 'when on timeout port returns nil' do
      it 'calls formatter with nil' do
        port.expect :gets, nil
        formatter.expect :from_line, nil, [nil]
        com.get
        formatter.verify
      end
    end
  end

  describe '#command' do
    it 'writes line' do
      port.expect :write, nil, ['dc 1 2']
      port.expect :write, nil, ["\n"]
      com.command 'dc', 1, 2
      port.verify
    end
  end

  describe '#until_done' do
    it 'calls get until there is "done"' do
      m1 = Minitest::Mock.new
      m1.expect :done?, false
      m2 = Minitest::Mock.new
      m2.expect :done?, true
      ary = [m1, m2, :should_not_call_done_on_this]
      com.stub :get, proc { ary.shift } do
        assert_equal [m1], com.until_done
      end
    end
  end
end
