require "minitest/autorun"

require_relative '../arguments'

describe Arguments do
  describe '::device' do
    describe 'when only one device' do
      it 'returns that device' do
        module A
          def self.find_all
            ['x0']
          end
        end
        assert_equal Arguments.device(A).call, 'x0'
      end
    end

    describe 'when more than one device' do
      it 'raises error' do
        module A
          def self.find_all
            ['x0', 'x1']
          end
        end
        assert_raises do
          Arguments.device(A).call
        end
      end
    end

    describe 'with argument' do
      module A
        def self.exist?(s)
          s == 'x0'
        end
      end

      it 'returns argument' do
        assert_equal Arguments.device(A).call('x0'), 'x0'
      end

      describe 'when device not found' do
        it 'raises error' do
          assert_raises do
            Arguments.device(A).call('x1')
          end
        end
      end
    end
  end

  describe '::defaults' do
    it 'includes defaults and options' do
      options = Arguments.defaults(x: 1)
      assert_equal 1, options[:x]
      assert options.has_key?(:dir)
      assert options[:dev].respond_to? :call
      assert_nil options[:plot][:xtics].call
    end

    describe 'with block' do
      it 'yields block' do
        options = Arguments.defaults do
          {x: bool}
        end

        assert options.has_key? :x
      end
    end
  end

  describe '::optional' do
    describe 'with symbol' do
      let(:option) { Arguments.optional(:bool) }

      it 'calls proc if argument given' do
        assert option.call('yes') == true
      end

      it 'returns nil if no argument given' do
        assert_nil option.call
      end
    end

    describe 'with block' do
      it 'calls block if argument given' do
        opt = Arguments.optional { |s| s.split ',' }
        assert_equal ['1'], opt.call('1,')
      end
    end
  end

  describe '::selector' do
    let(:option) { Arguments.selector }

    describe 'with ,' do
      it 'returns array' do
        assert_equal [1, 2], option.call('1,2')
      end
    end

    describe 'with selector' do
      it 'returns selector' do
        assert_equal 1.., option.call('1..')
      end
    end

    describe 'with number' do
      it 'returns array' do
        assert_equal [1], option.call('1')
      end
    end
  end

  describe '#sym' do
    it 'returns symbol' do
      c = Arguments.sym
      assert_equal :x, c.call('x')
    end
  end

  describe '#bool' do
    describe 'with default' do
      it 'returns bool' do
        c = Arguments.bool 'yes'
        assert_equal true, c.call
        assert_equal false, c.call('no')
      end
    end
  end
end
