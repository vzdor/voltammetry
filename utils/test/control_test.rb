require "minitest/autorun"

require_relative '../control'
require_relative '../prot'

describe Control::Dc do
  let(:com) { Minitest::Mock.new }

  let(:options) { {v: 100, delay: 21, dont_add_observers: true} }

  describe '#dc' do
    it 'calls dc delay/10 number of times, sleeping 10s between calls' do
      2.times do
        com.expect :dc, nil, [100]
        com.expect :sleep, nil, [10]
        com.expect :stop_done, []
      end
      com.expect :dc, nil, [100]
      com.expect :sleep, nil, [1]
      ctl = Control::Dc.new com, options
      ctl.dc
      com.verify
    end

    it 'notifies observers' do
      com.expect :dc, nil, [100]
      com.expect :sleep, nil, [10]
      com.expect :stop_done, [:not_this, :return_this_to_observer]
      com.expect :dc, nil, [100]
      ctl = Control::Dc.new com, options.merge(delay: 10)
      mock = Minitest::Mock.new
      mock.expect :run, nil, [:return_this_to_observer]
      mock.expect :stop, nil
      # ctl.observe(:run, :stop, mock), throws: unmocked method :method.
      ctl.observe(:run) { |*args| mock.run *args }
      ctl.observe(:stop) { mock.stop }
      ctl.dc
      mock.verify
    end

    describe 'with steps' do
      it 'steps up' do
        com.expect :values, nil
        com.expect :until_done, [Prot::Values.new(1, 1, ce: 50)]
        com.expect :dc, nil, [75]
        com.expect :sleep, nil, [1]
        com.expect :stop_done, nil
        com.expect :dc, nil, [100]
        ctl = Control::Dc.new com, options.merge(steps: 2, sdelay: 1, delay: 0)
        ctl.dc
        com.verify
      end
    end
  end

  it 'adds gnd observer' do
    options.delete :dont_add_observers
    com.expect :logger, Minitest::Mock.new
    com.expect :gnd, 1000
    ctl = Control::Dc.new com, options
    obs = ctl.observers[:run]
    observer = obs.find { |obs| obs.owner == Control::GndObserver }
    assert observer
  end

  describe 'with mf option' do
    it 'adds motor observer' do
      ctl = Control::Dc.new com, options.merge(mf: {i: 10})
      obs = ctl.observers[:run]
      observer = obs.find { |obs| obs.owner == Control::MotorObserver }
      assert observer
    end
  end
end

describe Control::GndObserver do
  let(:com) { Minitest::Mock.new }

  before do
    logger = Minitest::Mock.new
    com.expect :logger, logger
    logger.expect :error, nil, [String]
    logger.expect :info, nil, [String]
  end

  describe "#run" do
    let(:ctl) { Control::GndObserver.new com }

    before do
      com.expect :gnd, 1000
    end

    describe 'with gnd delta within interval' do
      it 'sets gnd' do
        com.expect :gnd=, nil, [{v: 1000}]
        vals = Prot::Values.new 1, 1, gnd: 1020
        ctl.run vals
        com.verify
      end
    end

    describe 'with gnd delta outside of interval' do
      it 'resists from setting gnd' do
        vals = Prot::Values.new 1, 1, gnd: 1101
        ctl.run vals
      end
    end
  end
end

describe Control::MotorObserver do
  let(:options) { {i: 5, dc: 10, step: 2} }

  let(:com) { Minitest::Mock.new }

  describe '#run' do
    describe 'when amp is low' do
      it 'steps up motor duty cycle' do
        com.expect :motor, nil, [{on: 1, dc: 12}]
        ctl = Control::MotorObserver.new com, options
        vals = Prot::Values.new 1, 1/1000.0
        ctl.run vals
        com.verify
      end

      describe 'with max option' do
        it 'steps motor dc to not more than max' do
          com.expect :motor, nil, [{on: 1, dc: 11}]
          ctl = Control::MotorObserver.new com, options.merge(max: 11)
          vals = Prot::Values.new 1, 1/1000.0
          ctl.run vals
          com.verify
        end
      end
    end

    describe 'when amp is high' do
      it 'steps down motor duty cycle' do
        com.expect :motor, nil, [{on: 1, dc: 8}]
        ctl = Control::MotorObserver.new com, options
        vals = Prot::Values.new 1, 9/1000.0
        ctl.run vals
        com.verify
      end
    end

    it 'adds entry to history' do
      ctl = Control::MotorObserver.new com, options
      vals = Prot::Values.new 1, 5/1000.0
      ctl.run vals
      assert_equal [10], ctl.history
    end
  end

  describe '#stop' do
    it 'saves history as com comment' do
      ctl = Control::MotorObserver.new com, options
      com.expect :comment, nil, ['mf dc history', "[1, 5]"]
      ctl.history << 1
      ctl.history << 5
      ctl.stop
      com.verify
    end
  end
end

describe Control::Cycle do
  let(:com) { Minitest::Mock.new }

  let(:options) { {fun: 1, delay: 50, dont_comment: true} }

  let(:reader) { Minitest::Mock.new }

  def cycle(ctl_block = nil, &block)
    ctl = Control::Cycle.new com, options
    Prot::ValuesReader.stub :new, reader do
      ctl_block.call ctl if ctl_block
      ctl.cycle &block
    end
  end

  def setup_reader_reply(reply = [])
    reply.each { |obj| reader.expect :next, obj }
    reader.expect :next, Prot::Comment.new('done')
  end

  describe '#cycle' do
    before do
      com.expect :cycle, nil, [options]
    end

    it 'calls com cycle with options' do
      setup_reader_reply
      cycle
      com.verify
    end

    it 'adds com comment' do
      options.delete :dont_comment
      com.expect :comment, nil do |name, value|
        assert_equal 'observed dt', name
        assert_equal 0, value
      end
      setup_reader_reply
      cycle
      com.verify
    end

    describe 'with values, iter and done' do
      let(:vals) { Prot::Values.new 1, 1 }

      let(:iter) { Prot::Comment.new 'iter' }

      before do
        setup_reader_reply [vals, iter]
      end

      it 'yelds values and iter' do
        ary = []
        cycle { |obj| ary << obj }
        assert_equal [vals, iter], ary
      end

      it 'sends iter to observers' do
        mock = Minitest::Mock.new
        mock.expect :call, nil, [nil]
        cycle proc { |ctl| ctl.observers[:iter] = [mock] }
        mock.verify
      end

      it 'sends values to observers' do
        mock = Minitest::Mock.new
        mock.expect :call, nil, [vals]
        cycle proc { |ctl| ctl.observers[:values] = [mock] }
        mock.verify
      end

      it 'sends done1 to observers' do
        mock = Minitest::Mock.new
        mock.expect :call, nil, [Array]
        cycle proc { |ctl| ctl.observers[:done1] = [mock] }
        mock.verify
      end
    end
  end
end

describe Control::PlotCycle do
  let(:options) { {dont_comment: true} }

  let(:com) { Minitest::Mock.new }

  let(:plot) { Minitest::Mock.new }

  # I have specs for Cycle and PlotCycle only binds to some
  # observer calls.

  describe 'when iter observed' do
    it 'adds new pary' do
      plot.expect :new_pary, [:new_pary]
      ctl = Control::PlotCycle.new com, plot, :add, options
      ctl.notify :iter
      plot.verify
    end

    describe 'when done1 observed' do
      it 'adds options and comments to pary' do
        pary = Minitest::Mock.new
        cmd = {}
        pary.expect :cmd, cmd
        pary.expect :cmd, cmd
        plot.expect :new_pary, pary
        ctl = Control::PlotCycle.new com, plot, :add, options
        ctl.notify :iter
        ctl.notify :done1, :some_comment
        assert_equal options, cmd[:options]
        assert_equal [:some_comment], cmd[:comments]
      end
    end
  end

  describe 'when values observed' do
    it 'adds values to plot.pary' do
      ary = []
      plot.expect :pary, ary
      ctl = Control::PlotCycle.new com, plot, :add, options
      ctl.notify :values, :some_obj
      assert_equal [:some_obj], ary
    end
  end
end
