require "minitest/autorun"

require_relative '../plot'

describe Firrol::Rolling do
  let(:ary) { Plot::Points.new }

  let(:rol) { Firrol::Rolling.new ary }

  describe '#roll' do
    it 'omits first and last `win` ys' do
      ary.add 1, 1
      ary.add 2, 2
      ary.add 3, 3
      rol.roll 1 do |ys|
        assert_equal [1, 2, 3], ys
        0
      end
      assert_equal [1, 0, 3], ary.map(&:y)
    end
  end
end
