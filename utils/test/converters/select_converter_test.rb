require "minitest/autorun"

require_relative '../../plot'
require_relative '../../converters/select_converter'

describe SelectConverter do
  let(:pary1) { Plot::Points.new }

  let(:pary2) { Plot::Points.new }

  let(:parys) { [pary1, pary2] }

  let(:c) { SelectConverter.new ['1'] }

  before do
    pary1 << Plot::Point.new(1, 2)
    pary1 << Plot::Point.new(3, 4)
    pary1.title = '1'
    pary2 << Plot::Point.new(1, 2)
    pary2 << Plot::Point.new(3, 4)
    pary2.title = '2'
  end

  it 'acts as enumerable' do
    c.add :select
    added, * = *c
    assert c.kind_of? Enumerable
  end

  describe '#add_self' do
    let(:c2) { SelectConverter.new }

    it 'returns self' do
      myself = c.add_self c2
      assert_equal c, myself
    end

    it 'adds' do
      c.add_self c2
      added, = *c
      assert_equal c2, added
    end
  end

  describe '#add' do
    describe 'converter' do
      let(:c2) { SelectConverter.new }

      it 'adds converter' do
        c.add c2
        added, = c.converters
        assert_equal c2, added
      end

      it 'returns new converter' do
        added = c.add c2
        assert_equal c2, added
      end
    end

    describe 'symbol' do
      it 'adds converter by symbol name' do
        c.add :select, ['1']
        added, = c.converters
        assert_equal SelectConverter, added.class
        assert_equal ['1'], added.sel
      end

      describe 'block given' do
        it 'ads select converter with block' do
          block = proc {}
          c.add :select, &block
          added, = c.converters
          assert_equal block, added.sel
        end
      end
    end
  end

  describe '#add_proc' do
    it 'adds proc converter' do
      block = proc { }
      c.add_proc &block
      assert_equal 1, c.converters.size
      added, = c.converters
      assert_equal block, added.block
    end
  end

  describe '#call' do
    it 'replaces selected parys' do
      assert_equal ['1'], c.sel
      c.add_proc { [] }
      new, *etc = c.call parys
      assert_equal 0, etc.size
      assert_equal new.title, '2'
    end

    it 'replaces equal? arrays, eql? arrays stay' do
      ary1 = [1, 2]
      ary2 = [1, 2]
      assert ary1.eql?(ary2)
      c = SelectConverter.new -> (ary) { ary.equal? ary2 }
      c.add_proc { [] }
      new = c.call [ary1, ary2]
      assert_equal [ary1], new
    end

    describe 'no sel' do
      it 'replaces all' do
        c = SelectConverter.new
        c.add_proc { [] }
        new = c.call parys
        assert_equal 0, new.size
      end
    end

    describe 'with add_map' do
      it 'applies map to arrays' do
        c = SelectConverter.new
        p = Plot::Point.new 0, 0
        c.add_map { |pary| [p] }
        new = c.call parys
        assert_equal [[p], [p]], new
      end
    end
  end
end
