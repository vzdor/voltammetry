require "minitest/autorun"

require_relative '../../plot'
require_relative '../../converters/cut_converter'

describe CutConverter do
  let(:pary) { Plot::Points.new }

  let(:lxs) { [1, 2] }

  let(:rxs) { [3, 4] }

  before do
    lxs.each { |x| pary << Plot::Point.new(x, 1) }
    rxs.each { |x| pary << Plot::Point.new(x, 2) }
  end

  it 'deletes points from zero to middle' do
    ary, = CutConverter.new(:left).call [pary]
    assert ary
    assert_equal rxs, ary.map(&:x)
  end

  it 'deletes points from middle to end' do
    ary, = CutConverter.new(:right).call [pary]
    assert_equal lxs, ary.map(&:x)
  end
end
