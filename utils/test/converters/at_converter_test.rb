require "minitest/autorun"

require_relative '../../plot'
require_relative '../../converters/at_converter'

describe AtConverter do
  let(:pary) { Plot::Points.new }

  let(:c) { AtConverter.new 0.4, 0.1001 }

  before do
    [[0.1, 1], [0.2, 1], [0.3, 1],
     [0.4, 5], [0.5, 1], [0.6, 1]].each do |x, y|
      pary.add x, y
    end
  end

  it 'filters v +/- dv' do
    parys = c.call [pary]
    assert parys
    assert_equal 1, parys.size
    p1, p2, p3 = parys.first
    assert_equal 0.3, p1.x
    assert_equal 0.4, p2.x
    assert_equal 0.5, p3.x
  end

  it 'clones parys' do
    pary.title = 'cu'
    p, = c.call [pary]
    assert_equal 'cu', p.title
  end
end
