require 'minitest/autorun'

require_relative '../../prot'
require_relative '../../meter_plot'
require_relative '../../converters/ips_converter'

describe IpsConverter do
  let(:pary) { MeterPlot::Points.new }

  before do
    vals = Prot::Values.new(1, 0.0005)
    vals.ips << Prot::Ip.new(0.0002)
    vals.ips << Prot::Ip.new(0.0001)
    vals.ips << Prot::Ip.new(0.0004)
    vals.ips << Prot::Ip.new(0.0002)
    pary << vals
  end

  describe 'with show' do
    it 'adds ip points to pary' do
      c = IpsConverter.new show: true
      new_ary, = c.call [pary]
      assert_equal 4, new_ary.size
    end

    describe 'with on' do
      it 'returns selected' do
        c = IpsConverter.new show: true, on: [2]
        new_ary, = c.call [pary]
        assert_equal 1, new_ary.size
        p, = new_ary
        assert_equal 0.0004, p.y
      end
    end

    describe 'with empty ips' do
      it 'returns empty pary' do
        c = IpsConverter.new show: true
        pary[0].ips = []
        new_ary, = c.call [pary]
        assert new_ary.empty?
      end
    end
  end

  describe 'with square' do
    before do
      vals, = pary
      # Mark as square.
      pary.cmd[:options] = {fun: 1}
    end

    it 'computes pulse diff' do
      c = IpsConverter.new
      new_ary, = c.call [pary]
      assert_equal 1, new_ary.size
      p, = new_ary
      assert_in_delta -1.5, p.y * 10**4
    end


    describe 'with on' do
      it 'computes pulse diff only from selected ips' do
        c = IpsConverter.new on: [1]
        new_ary, = c.call [pary]
        p, = new_ary
        assert_equal -1, p.y * 10**4
      end
    end
  end
end
