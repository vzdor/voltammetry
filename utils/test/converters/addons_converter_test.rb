require 'minitest/autorun'

require_relative '../../converters/addons_converter'
require_relative '../../plot'

describe AddonsConverter do
  let(:c) { AddonsConverter.new }

  let(:pary) { Plot.new_pary }

  before do
    pary.title = 'plot1'
  end

  describe 'x2y1' do
    let(:options) { {x2y1: [pary.title]} }

    it 'adds commands' do
      c.call [pary], options
      assert_equal [:x2tics], options[:commands]
    end

    it 'sets pary axes' do
      c.call [pary], options
      assert_equal 'axes x2y1', pary.plotline.first
    end
  end
end
