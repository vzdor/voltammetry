require "minitest/autorun"

require_relative '../../plot'
require_relative '../../converters/avg_converter'

describe AvgConverter do
  let(:win) { 2 }

  let(:converter) { AvgConverter.new win }

  it 'applies simple median' do
    pary = Plot::Points.new
    pary.add 1, 1
    mock = Minitest::Mock.new
    mock.expect :call, -> (el) { _(el).must_equal pary }, [:rolling, :median, win]
    Firrol.stub :[], mock do
      converter.call! [pary]
    end
    mock.verify

    # The below tests algorithm, but not fully - different win values.

    # pary = Plot::Points.new
    # pary.add 1, 2
    # pary.add 2, 1
    # pary.add 3, 2
    # converter.call! [pary]
    # _, middle, _ = pary
    # assert_equal 2, middle.y
  end
end
