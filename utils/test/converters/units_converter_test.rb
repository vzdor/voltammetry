require "minitest/autorun"

require_relative '../../plot'
require_relative '../../converters/units_converter'

describe UnitsConverter do
  let(:pary) { Plot::Points.new }

  let(:c) { UnitsConverter.new }

  describe 'with millis' do
    it 'sets ylabel/multiplier to milli' do
      pary.add 1, 1
      options = {units: 'A'}
      c.call [pary], options
      assert_equal 'mA', options[:ylabel]
      assert_equal 1, options[:multiply]
    end
  end

  it 'sets ylabel/multiplier to micro' do
    pary.add 1, 0.01
    options = {units: 'A'}
    c.call [pary], options
    assert_equal 'uA', options[:ylabel]
    assert_equal 1000, options[:multiply]
  end

  describe 'with empty pary' do
    it 'sets ylabel to micro' do
      options = {units: 'A'}
      c.call [pary], options
      assert_equal 'uA', options[:ylabel]
    end
  end
end
