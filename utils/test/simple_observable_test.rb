require "minitest/autorun"

require_relative '../simple_observable'

describe SimpleObservable do
  let(:obj) { Object.new }

  before do
    obj.extend SimpleObservable
    obj.simple_observable
  end

  describe '#observe' do
    it 'adds proc' do
      p = proc { }
      obj.observe :done, &p
      assert_includes obj.observers[:done], p
    end

    it 'adds object method' do
      mock = Minitest::Mock.new
      mock.expect :method, :method_done, [:done]
      obj.observe :done, mock
      assert_includes obj.observers[:done], :method_done
      mock.verify
    end

    it 'returns block' do
      block = obj.observe(:done) {}
      assert_includes obj.observers[:done], block
    end
  end

  describe '#notify' do
    it 'calls block' do
      called = []
      obj.observers[:done] = [proc { |x| called << x }]
      obj.notify :done, 1
      assert_equal [1], called
    end

    describe 'when observer is deleted from block' do
      it 'continues iterating' do
        called = []
        p1 = proc { obj.observers[:done].delete p1 }
        p2 = proc { called << 1 }
        obj.observers[:done] = [p1, p2]
        obj.notify :done
        assert_equal [1], called
      end
    end
  end
end
