require "minitest/autorun"

require_relative '../plot'

describe Plot::Settings do
  let(:settings) { Plot::Settings.new }

  describe '#to_s' do
    it 'includes linetypes' do
      settings.linetype 5, lw: 1
      assert_includes settings.to_s, 'set linetype 5 lw 1'
    end

    it 'includes key values' do
      settings[:x1] = 1
      settings[:x2] = nil
      str = settings.to_s
      assert_includes str, 'set x1 1'
      assert_includes str, 'set x2'
    end
  end

  describe '#terminal' do
    it 'returns terminal' do
      assert_kind_of Plot::Settings::Terminal, settings.terminal
    end
  end

  describe '#set' do
    it 'adds key value' do
      settings.set :x1, 1
      assert_equal '1', settings[:x1]
    end
  end

  describe '#colors_16' do
    it 'adds linetypes' do
      settings.colors_16
      assert_equal 16, settings.linetypes.size
      ls = settings.linetypes
      assert_nil ls[1].dt
      assert_equal 2, ls[2].dt
      assert_equal ls[2].lc, ls[1].lc
    end
  end
end

describe Plot::Points do
  let(:pary) { Plot::Points.new }

  describe '#add' do
    it 'adds new point' do
      pary.add 1, 2
      p, = pary
      assert p.kind_of? Plot::Point
      assert_equal [1, 2], [p.x, p.y]
    end
  end

  describe '#empty' do
    it 'returns empty copy' do
      obj = pary.empty
      assert obj.__id__ != pary.__id__
      assert obj.empty?
      assert_equal obj.style.__id__, pary.style.__id__
    end
  end

  describe '#copy!' do
    it 'clones attributes' do
      style = pary.style
      pary.copy! :style
      assert style.__id__ != pary.style.__id__
    end
  end

  describe '#style' do
    it 'returns style' do
      pary.style ps: 1
      assert_equal 1, pary.style.ps
    end

    it 'updates attrs' do
      pary.style :points, ps: 1
      pary.style dt: 2
      assert_equal 1, pary.style.ps
      assert_equal 2, pary.style.dt
    end
  end

  describe '#to_s' do
    before do
      pary.add 1, 2
      pary.add 3, 4
    end

    it 'serializes points' do
      assert_equal "1 2\n3 4", pary.to_s
    end
  end
end

describe Plot::Points::Style do
  let(:style) { Plot::Points::Style.new }

  describe '#color=' do
    it 'sets lc' do
      style.color = 'black'
      assert_equal "rgb 'black'", style.lc
    end
  end

  describe '#set' do
    it 'updates attrs' do
      style.dt = 2
      style.set :linespoints, color: 'black'
      assert_equal 2, style.dt
      assert_equal :linespoints, style.type
      assert_equal "rgb 'black'", style.lc
    end
  end

  describe '#to_s' do
    before do
      style.ps = 1
      style.dt = 2
      style.lw = 1
    end

    describe 'lines' do
      it 'returns line members' do
        style.type = :lines
        assert_equal 'lines dt 2 lw 1', style.to_s
      end
    end

    describe 'points' do
      it 'returns point members' do
        style.type = :points
        assert_equal 'points dt 2 ps 1', style.to_s
      end
    end

    describe 'linespoints' do
      it 'returns all members' do
        style.type = :linespoints
        assert_equal 'linespoints dt 2 ps 1 lw 1', style.to_s
      end
    end

    it 'prefers values from options' do
      style.type = :points
      assert_includes style.to_s(type: :lines), 'lines'
    end
  end

  describe '#lines, #points, #linespoints' do
    it 'sets type' do
      [:lines, :points, :lines].each do |k|
        style.send k, dt: 2
        assert_equal k, style.type
        assert_equal 2, style.dt
      end
    end
  end
end

describe Plot::Point do
  describe '#to_a' do
    it 'returns [x, y] array' do
      p = Plot::Point.new 1, 2
      x, y = *p
      assert_equal [1, 2], [x, y]
    end
  end
end

describe Plot do
  let(:plot) { Plot.new }

  describe "#to_s" do
    it "returns plot format" do
      fmt =<<EOT
set grid back
set terminal wxt size 512,384 noraise font "Monospace,11"

plot '-' using 1:($2 * 1) with lines title '0', '-' using 1:($2 * 1) with lines title '1'
1 2
3 4
e
1 2
3 4
e
EOT
      pary = plot.new_pary
      pary.add 1, 2
      pary.add 3, 4
      pary = plot.new_pary
      pary.add 1, 2
      pary.add 3, 4
      assert_equal fmt, plot.to_s
    end

    it 'includes xtics' do
      assert_includes plot.to_s(xtics: 'rotate'), 'set xtics rotate'
    end

    it 'includes ytics' do
      assert_includes plot.to_s(ytics: '0.1'), 'set ytics 0.1'
    end

    it 'includes y multiplier' do
      pary = plot.new_pary
      pary.add 1, 2
      assert_includes plot.to_s(multiply: 100), 'using 1:($2 * 100)'
    end

    it 'includes xlabel' do
      assert_includes plot.to_s(xlabel: 'v'), 'set xlabel "v"'
    end

    it 'includes ylabel' do
      assert_includes plot.to_s(ylabel: 'q'), 'set ylabel "q"'
    end

    it 'includes title' do
      pary = plot.new_pary
      pary.title = 'cu'
      pary.add 1, 2
      assert_includes plot.to_s, "title 'cu'"
    end

    it 'includes xlabel from #new options' do
      plot = Plot.new xlabel: 'PGE'
      assert_includes plot.to_s, 'PGE'
    end

    it 'includes pary style' do
      pary = plot.new_pary
      assert_includes plot.to_s, pary.style.to_s
    end

    it 'adds commands' do
      s = plot.to_s commands: ['key opaque', 'x2label "V"']
      assert_includes s, 'set key opaque'
      assert_includes s, 'set x2label'
    end

    it 'calls converter' do
      called = false
      plot.converters << proc { |parys| called = true; parys }
      plot.to_s
      assert called
    end
  end

  describe "#to_png" do
    it "includes file options" do
      pary = plot.new_pary
      pary.add 1, 2
      s = plot.to_png 'file.png'
      assert_includes s, "set output 'file.png'"
      assert_includes s, "set terminal pngcairo"
    end
  end

  describe '#new_pary' do
    it 'returns pary' do
      pary = Plot.new_pary
      assert pary.kind_of? Plot::Points
    end
  end
end

describe Plot::Renderer do
  let(:plot) { Plot.new }

  let(:gp) { '' }

  let(:renderer) { Plot::Renderer.new gp, plot }

  describe 'with parys' do
    before do
      pary = plot.new_pary
      pary.add 1, 1
      pary.add 1, 2
    end

    it 'renders settings once' do
      renderer.render
      assert_includes gp, 'set term'
      gp.clear
      renderer.render
      refute_includes gp, 'set term'
    end

    describe 'with something new in options' do
      it 'renders settings' do
        renderer.render
        gp.clear
        renderer.render new_key: true
        assert_includes gp, 'set term'
      end
    end
  end

  describe 'with first empty pary' do
    before do
      plot.new_pary
    end

    it 'renders nothing' do
      renderer.render
      assert_equal gp, ''
    end
  end
end
