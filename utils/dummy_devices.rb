module DummyDevices
  class << self
    def find_all
      []
    end

    def exist?(s)
      true
    end
  end
end
