module Defp
  class Procs
    class Context < Struct
      attr_accessor :blocks

      class Block < Struct.new(:args, :kw, :block)
        def where?(*args)
          where = kw[:where]
          where.nil? || match?(where, *args)
        end

        protected

        def match?(where, *args)
          v = args.first
          w = where.values.first
          if w.respond_to? :call
            w.call v
          else
            w == v
          end
        end
      end

      def self.new(*args)
        args = args.dup
        args << :block
        super *args
      end

      def initialize(*args)
        super
        @blocks = []
      end

      def run(*args)
        block = find *args
        instance_exec *args, &block.block
      end

      def find(*args)
        @blocks.detect { |block| block.where? *args }
      end

      def copy(*args)
        keep = values[args.size..] || []
        c = self.class.new *(args + keep)
        c.blocks = @blocks
        c
      end

      def add_block(*args, **kw, &block)
        @blocks << Block.new(args, kw, block)
      end
    end

    def initialize(obj)
      @map = Hash.new
      @obj = obj
    end

    def context(name, **kw)
      @map[name] ||= new_context name, **kw
    end

    def new_context(name, **kw)
      context = Context.new(*kw.keys).new *kw.values
      @obj.define_method name do |*args, &block|
        copy = context.copy *args
        copy.block = block
        copy.method :run
      end
      context
    end
  end

  def defp(name, *args, **kw, &block)
    @defp ||= Procs.new self
    context = @defp.context name, **kw
    if block
      context.add_block *args, **kw, &block
    end
  end
end
