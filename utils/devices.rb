require 'open3'

class UsbSerial
  attr_reader :tty, :id

  def initialize(tty)
    @tty = tty
    @id = tty_id
  end

  def attr(name)
    m = text.match /#{name}\s+\w+\s+(.+)/i
    m[1]
  end

  def idproduct
    attr :idproduct
  end

  def iproduct
    attr :iproduct
  end

  def text
    @text, _ = Open3.capture2e('lsusb', '-v', '-d', id) unless @text
    @text
  end

  def self.from_file(file)
    *, tty = file.split '/'
    new tty
  end

  protected

  def tty_id
    s = File.read("/sys/bus/usb-serial/devices/#{tty}/../uevent")
    m = s.match /PRODUCT=(\w+)\/(\w+)/
    # Vendor:proudct.
    m[1..2].join ':'
  end
end

module Devices
  IPRODUCTS = [
    'USB2.0-Serial'
    # Generic CH340B text.
    #  CH340B (not the G) has an EEPROM.
    #  https://www.mpja.com/download/35227cpdata.pdf
    #  Tool for EEPROM writting:
    #    http://www.downxia.com/downinfo/196126.html
    #
    # https://arduino.stackexchange.com/questions/6617/setting-serial-number-on-ch340-usb-serial-device
    #
    # TODO: Try to change text, requires Windows computer.
  ]

  class << self
    def find_all
      files = Dir.glob '/dev/ttyUSB[0-9]*'
      files.filter do |f|
        serial = UsbSerial.from_file f
        serial.idproduct.include?('CH340') && IPRODUCTS.include?(serial.iproduct)
      end
    end

    def exist?(file)
      File.exists? file
    end
  end
end
