
require_relative 'simple_observable'

module Control
end

# c = Cycle.new
# c.observe :values { |vals| ... }
# c.observe :done { ... }
# c.cycle
class Control::Cycle
  include SimpleObservable

  def initialize(com, options)
    @com = com
    @options = options
    simple_observable
    add_observers
  end

  def cycle(&block)
    n = 1
    @com.cycle @options
    dt = measure do
      until (obj = get).done?
        notify :obj, obj
        block.call obj if block
        n += 1
      end
    end
    notify :done, [dt, n]
  end

  protected

  def get
    reader = Prot::ValuesReader.new
    reader.next { @com.get }
  end

  def measure(&block)
    t1 = Time.now
    block.call
    t2 = Time.now
    t2 - t1
  end

  def add_observers
    observe :obj do |obj|
      case obj.name
      when 'iter'
        notify :iter
      when 'values'
        notify :values, obj
      end
    end
    observe :done do |dt, n|
      comment = ['observed dt', (dt / n * 1000).floor]
      @com.comment *comment unless @options.has_key? :dont_comment
      notify :done1, comment
    end
  end
end

class Control::PlotCycle < Control::Cycle
  def initialize(com, plot, *keys, options)
    super com, options
    @plot = plot
    @keys = keys
    add_plot_observers
  end

  def add?
    @keys.include? :add
  end

  protected

  def add_plot_observers
    observe_if add?, :values do |vals|
      @plot.pary << vals
    end
    observe_if add?, :iter do
      pary = @plot.new_pary
      # Save experiment options for every array of points.
      block = observe :done1 do |comment|
        pary.cmd[:options] = @options
        pary.cmd[:comments] = [comment]
        # Stop observing done1 for this pary.
        observers[:done1].delete block
      end
    end
  end
end

class Control::Dc
  include SimpleObservable

  def initialize(com, options)
    @options = options
    @com = com
    @delay = options[:delay] || 0
    @v = options[:v]
    @steps = options[:steps] || 0
    @sdelay = options[:sdelay] || 1
    simple_observable
    add_observers
  end

  def dc
    step_dc if @steps > 0
    _dc
  end

  protected

  def step_dc
    @com.values
    *, vals = @com.until_done
    (1..(@steps - 1)).each do |i|
      @com.dc vals.ce + (@v - vals.ce)/@steps * i
      # @com.sleep can be stubbed in tests.
      @com.sleep @sdelay
      @com.stop_done
    end
  end

  def _dc
    t = 10
    run_number = @delay/t
    (1..run_number).each do
      @com.dc @v
      @com.sleep t
      *, vals = @com.stop_done
      notify :run, vals
    end
    notify :stop
    @com.dc @v
    end_t = @delay % t
    @com.sleep end_t if end_t > 0
  end

  def add_observers
    ary = []
    unless @options.has_key? :dont_add_observers
      ary << Control::GndObserver.new(@com)
    end
    ary += @options[:observers] if @options.has_key? :observers
    if @options.has_key? :mf
      ary << Control::MotorObserver.new(@com, @options[:mf])
    end
    ary.each { |observer| observe :run, :stop, observer }
  end
end

# Tries to keep gnd fixed.
# In a long dc process gnd can shift due to RC-type DAC. If error is not
# minor, ignore it, it is likely due to TIA limited ability
# to handle current.
class Control::GndObserver
  def initialize(com)
    @com = com
    @gnd = com.gnd
    @logger = com.logger
    @interval = (10..100)
  end

  def run(vals)
    dv = (vals.gnd - @gnd).abs
    if dv > @interval.max
      @logger.error "GND dv = #{dv} > #{@interval.max}, ignoring."
    elsif @interval.include? dv
      @logger.info "GND dv = #{dv}, adjusting."
      @com.gnd = {v: @gnd}
    end
  end

  def stop(*args)
  end
end

class Control::MotorObserver
  attr_reader :history

  def initialize(com, options)
    @com = com
    @i = options[:i]
    @dc = options[:dc] || 10
    @dc_step = options[:step] || 2
    @dc_max = options[:max] || 50
    @history = []
  end

  def run(vals)
    i = (vals.amp * 1000).floor
    di = @i - i.abs
    if di.abs > 3
      @dc += di.abs/di * @dc_step
      @dc = [@dc_max, @dc].min
      @dc = [0, @dc].max
      @com.motor on: 1, dc: @dc
    end
    @history << @dc
  end

  def stop(*args)
    @com.comment 'mf dc history', @history.to_s
  end
end
