require_relative '../experiment'
require_relative '../arguments'

scheme = Arguments.defaults do
  {
    dev: optional(:device),
    # STAIRCASE.
    #
    # Begin at t0 ms. Set voltage, v, to e0. Wait dt ms,
    # set v to v - dv. So on n times.
    sc: {
      t0: 0,
      v0: 550,
      dt: 50,
      dv: 50,
      n: 10
    }
  }
end

experiment(scheme, title: 'Spice pwl.') do
  # 0 300mV
  # 0.01ms 250mV 50.01ms 250mV
  # .. etc.
  sc = options[:sc]
  t0, v0, dt, dv, n = sc.values_at :t0, :v0, :dt, :dv, :n
  tt = 0.01
  ary = [0, v0] + (1..n).map do |i|
    v = v0 - dv * i
    [
      t0 + dt * (i - 1) + tt, v,
      t0 + dt * i, v
    ]
  end
  s = ary
        .flatten
        .each_slice(2)
        .map { |t, v| "#{t}ms #{v}mV" }.join ' '
  tell "ALTER @v1[pwl] = [ #{s} ]"
end
