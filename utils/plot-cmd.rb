require_relative 'experiment'
require_relative 'arguments'
require_relative 'pary_files'
require_relative 'converters/all'

scheme = Arguments.defaults do
  {
    combine: {
      on: selector,
      omit: optional(:selector),
    },
    dev: optional(:device),
    iter: selector,
    at: optional { |s| s.split(',').map(&:to_f) },
    cut: optional(:sym),
    smm: {
      on: optional(:int),
      only: optional { |s| s.split ',' }
    },
    titles: optional { |s| s.split ',' },
    view: optional(:bool),
    ips: {
      on: optional(:selector),
      show: bool
    },
    filter: {
      on: optional(:sym),
      cutoff: 30
    }
  }
end

experiment(scheme, title: 'Plot command.') do
  pre = SelectConverter.new

  if options[:ips].has_key? :on
    pre.add :ips, options[:ips]
  end

  if options[:filter].has_key? :on
    pre.add :filter, options[:filter]
  end

  if options[:smm].has_key? :on
    win, only = options[:smm].values_at :on, :only
    pre.add(:select, only).add(:avg, win)
  end

  pf = ParyFiles.new dumpster
  pf.all(options[:combine], options) do |parys|
    plot.parys.concat pre.call parys
  end

  if options.has_key? :titles
    options[:titles].zip(plot.parys) { |title, ary| ary.title = title }
  end

  if options.has_key? :cut
    plot.converters << CutConverter.new(*options[:cut])
  end

  if options.has_key? :at
    plot.converters << AtConverter.new(*options[:at])
  end

  fp = dumpster.fp('combined.png')
  gp.puts plot.to_png fp

  gp.puts plot.to_s if options.has_key? :view
end
