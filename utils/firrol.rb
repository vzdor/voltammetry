# coding: utf-8
module Firrol
  class Fir
    attr_reader :ary

    def initialize(ary)
      @ary = ary
    end

    # Without any knowledge on signal processing, I found
    # b coefficients using `dfcgen` program:
    #  FIR
    #  f_{sample} = 100 Hz
    #  f_{cutoff} = 10 - 21 Hz.

    # The lowpassN, N - filter order.

    # Testing.
    # These look ok with rolling median filter:
    #  --dir fc20 --combine 3..
    #  --dir cu-bt1 --combine 1..
    # Not ok with median, ok with lowpass filter:
    #  --dir fc23 --combine 16,17,19,20 --ips-sw --ips-only 11
    #                                            --ips-only 10
    # Not ok:
    #                                            --ips-only 9

    # On digital filters:
    # https://123.physics.ucdavis.edu/week_5_files/filters/digital_filter.pdf

    # lowpass8, at 17 Hz cutoff is good for:
    #  --dir cu-bt1 --combine 1..
    #  delay: 60 + oversampling = 85 ms
    #  dv: 15.
    # At 15 Hz is ok for: 10 dv, 80 ms, like --dir fc20. "Noisy" at
    # 17 Hz.

    # Will "overshoot" at 17 Hz: 5.times { lowpass8 }.

    # require 'bigdecimal'

    def lowpass8(i = 1)
      coefs_db = [
        # b0, b1, b2, b3. b4, b5, ..., b8 will be found by new_filter.
        [-0.0713404, -0.00660089, 0.133141, 0.276367], # 17 Hz cutoff.
        [-0.0584237, 0.0126711, 0.137216, 0.256083], # 16 Hz.
        [-0.0429205, 0.0300862, 0.138894, 0.2363], # 15 Hz.
        [-0.025997, 0.045362, 0.138739, 0.217655], # 14 Hz.
        [0.00847431, 0.0694636, 0.134962, 0.185141], # 12 Hz.
      ]
      coefs = coefs_db[i]
      # 17 Hz.
      # b0 = BigDecimal('-0.0713404')
      # b1 = BigDecimal('-0.00660089')
      # b2 = BigDecimal('0.133141')
      # b3 = BigDecimal('0.276367')
      # b4 = BigDecimal('0.336867')

      filtfilt 9, &new_filter(coefs)
    end

    def lowpass4(b0 = 0.135418, b1 = 0.230386)
      b2 = 1 - b0 * 2 - b1 * 2
      b3 = b1
      b4 = b0
      filtfilt(5) do |y1, y2, y3, y4, y5|
        y1 * b0 + y2 * b1 + y3 * b2 + y4 * b3 + y5 * b4
      end
    end

    def lowpass2(b0 = 0.301086)
      b1 = 1 - b0 * 2
      b2 = b0
      filtfilt(3) { |y1, y2, y3| y1 * b0 + y2 * b1 + y3 * b2 }
    end

    # coefs: first N/2 coefficients. b's.
    #
    # N + 1 coefficient, required for FIR filter of degree N,
    # can be built from `coefs`:
    # n0, b1, ..., b_{middle}, ..., b1, b0.
    # Seem b0 + b1 + ... = 1. So b_{middle} = 1 - 2 * b0 - 2 * b1 - ...
    def new_filter(coefs)
      middle = 1 - coefs.sum { |b| 2 * b }
      proc do |ys|
        s = (0..coefs.size - 1).sum do |i|
          b = coefs[i]
          b * ys[i] + b * ys[ys.size - 1 - i]
        end
        s + middle * ys[ys.size/2]
      end
    end

    def filtfilt(n, &filter)
      # If n = 3, for every i < size, #filt will call
      #  filter(y_i, y_{i + 1}, y_{i + 2}) = f0_i.
      # If i = size - 1 = e0
      #  filter(y_e0, y_e0, y_e0) = f0_e0 = y_e0, see #filt.
      # Then #filt is called backwards, for every i >= 0
      #  filter(f0_i, f0_{i - 1}, f0_{i - 2}) = f1_i.

      # If i = e0, backwards
      #  filter(f0_e0, f0_{e0 - 1}, f0_{e0 - 2}) = f1_e0 =
      #  = filter(y_e0, ...).
      # f1_e0 is not desired. f1_i is not desired if
      #  f0_i = y_i or f0_{i - 1} = y_{i - 1} or ...
      # and f1_i =/= y_i.

      # If backwards pass will begin from e0 - 1
      #  f0_{e0 - 1}, f0_{e0 - 2}, ...
      # OK.

      # At i = 0
      #  filter(f0_0, f0_0, f0_0) = f1_0 = f0_0.
      # f1_0 is not desired.

      # If forward pass will begin from 1, then for backward pass
      # at i = 0
      #  filter(f0_0, ...) = filter(y_0, y_0, y_0) = y_0.
      # OK.
      # If i = 1
      #  filter(f0_1, f0_0, f0_0) = filter(f0_1, y_0, y_0).
      # Not OK.

      # If y0 is inserted into array, so
      #  y0' = y0, y1' = y0, y2' = y1, ...
      # This element will be deleted from filtered array, so
      # what will be assigned to it is not relevant.

      # i = 0 is not important, i = 1
      #  filter(f0_1, f0_0, f0_0) = filter(f0_1, y_0, y_0)
      # Not OK.

      # If two more y0 inserted, i = 3
      #  filter(f0_3, f0_2, f0_1)
      # OK.
      ys0 = [ary[0]] * n
      ary.insert 0, *ys0
      filt 1.upto(ary.size - 1), n, &filter
      filt (ary.size - 2).downto(0), -n, &filter
      ary.shift n
      ary
    end

    # `n` is window size, but can be negative and in this
    # case select y's in backward order:
    #  [y_i, y_{i - 1}, ..., y_{i - win}]
    def filt(iter, n = 1, ry = ary, &block)
      win = n.abs
      iter.each do |i|
        # filter_map will continue if result is false. Why
        # there is no map_while?
        # ys = (0..win - 1).filter_map do |k|
        #   ii = i + k * win/n
        #   ry[ii].y if iter.include? ii
        # end
        ys = []
        (0..win - 1).each do |k|
          ii = i + k * win/n
          break unless iter.include? ii
          ys << ry[ii].y
        end
        # If ys.size == 1, it does not make sense to add
        # y's and call filter: b0 y1 + b1 y1 + b2 y1 = y1.
        next if ys.size < 2
        # Add y's if less than required window.
        ys << ys[-1] while ys.size < win
        # Call filter and assign value to y_i.
        y = block.call ys
        ry[i] = Plot::Point.new ry[i].x, y
      end
      ry
    end
  end

  class Rolling
    attr_reader :ary

    # Rolling means traversing array of points, @ary, by:
    #  x_i = [p_{i - win}, ..., p_i, ... , p_{i + win}]
    # The result of roll block is assigned to p_i.

    def initialize(ary)
      @ary = ary
    end

    # Simple moving median.
    def median(win)
      roll(win) { |ys| ys.sort[win] }
    end

    # Select p_k from X = [p_{i - 1}, p_i, p_{i + 1}] with smaller
    # distance to any p_n in X, n != k and k != i.
    def close1
      roll 1 do |y1, y2, y3|
        el, * = [
          [(y2 - y1).abs, y1],
          [(y2 - y3).abs, y3],
          [(y1 - y3).abs, y1]
        ].sort_by &:first
        el[-1]
      end
    end

    def close2
      roll 2 do |ys|
        y1, y2, y3, y4, y5 = ys
        el, * = [
          [(y2 - y3).abs, y2],
          [(y4 - y3).abs, y4],
          [(y1 - y5).abs, y1],
          [(y2 - y4).abs, y2]
        ].sort_by &:first
        el[-1]
      end
    end

    def outliers2
      roll 2 do |ys|
        y1, y2, y3, y4, y5 = ys
        el, * = [
          # ys.sort[2] is median. y3 should be included for
          # cases like: [1, 0, 1, 0, 1].
          [(y1 + y2 - (y4 + y5)).abs, ys.sort[2]],
          [(y2 - y3).abs, y3],
          [(y4 - y3).abs, y3],
          [(y1 - y3).abs, y3],
          [(y5 - y3).abs, y3]
        ].sort_by &:first
        el[-1]
      end
    end

    def roll(win, iter = (win..ary.size - win - 1), &block)
      iter.each do |i|
        sel = (-win..win).map { |n| ary[i + n] }
        y = yield sel.map(&:y)
        ary[i] = Plot::Point.new ary[i].x, y
      end
      ary
    end
  end

  class << self
    # If I want to apply a filter to every array element:
    #  ary.map { |e| Fir.new(e).lowpass }
    # Looks ok, but if lowpass method was a class method, I could do:
    #  ary.map &Fir.method(:lowpass)
    #
    # If I add global obj_2_proc:
    #  ary.map &obj_2_proc(Fir, :lowpass)
    # It looks like it can be useful somewhere else:
    #  urls.map &obj_2_proc(Http, :get)
    # but this may throw 404 error for some urls.. catch and continue..
    # Not so trivial.

    # ary.map &Firrol[:rolling, :outliers]
    # ary.map &Firrol[:fir, :lowpass4, *coefs]
    def [](key, method, *args)
      klass = const_get key.capitalize
      proc { |ary| klass.new(ary).send method, *args }
    end
  end

end
