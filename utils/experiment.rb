require 'bundler'
Bundler.setup
require 'serialport'
require 'fileutils'
require 'json'

require_relative 'com'
require_relative 'control'
require_relative 'meter_plot'
require_relative 'converters/units_converter'
require_relative 'converters/addons_converter'
require_relative 'simple_options'
require_relative 'gp_formatter'

# Ties plot, arguments, com and com controllers (Cycle, Dc, etc).
class Experiment
  attr_reader :dumpster, :options, :c

  def initialize(com, dumpster, options = {})
    @c = com
    @dumpster = dumpster
    @options = options
    @quiet = options[:quiet]
    @plot_options = options[:plot]
    tell @c.meta0 unless quiet? || @c.nil?
    @gp = options.delete :gp if options.has_key? :gp
  end

  def com?
    @c
  end

  def gp
    # Sometimes on first run, the window shows odd fonts and console
    # warning: slow font initialization...
    # With --slow it will wait for font init...
    @gp ||= IO::popen 'gnuplot --slow -p', 'w+'
  end

  def plot
    @plot ||= MeterPlot.new @plot_options
  end

  def quiet?
    @quiet
  end

  def tell(s)
    $stdout.puts s
  end

  def ask
    $stdin.gets
  end

  def comments
    c.meta0[2..]
  end

  def save(formatter: GpFormatter)
    serializer = JSON.method :pretty_generate
    dumpster.save do |n, dump|
      gp.puts plot.to_png dumpster.fp("plot-#{n}.png")
      dump << ["meta0-#{n}.txt", serializer.call(c.meta0)] if com?
      dump << ["options-#{n}.txt", serializer.call(options)]
      plot.parys.each_with_index do |pary, i|
        file = "#{n}-#{i}.pary"
        dump << [file, formatter.dump(pary)]
        dump << [file + '-cmd', serializer.call(pary.cmd)]
      end
    end
  end

  # cycle :add
  # cycle :add, :plot
  # cycle :add, :plot, delay: 10
  # Will call PlotCycle.new .., :plot, options[:cycle].merge(deley: 10).
  # cycle :add, options[:cycle2]
  # Will call PlotCycle.new .., :plot, options[:cycle2].
  # So the arguments order is similar to PlotCycle.new.
  def cycle(*keys, **kw, &block)
    _options = keys.last
    _options = options[:cycle] unless _options.kind_of? Hash
    ctl = Control::PlotCycle.new c, plot, *keys, _options.merge(kw)
    ctl.cycle &block
  end

  # dc
  # dc delay: 10
  # dc options[:idle]
  # dc options[:idle], delay: 10
  # Will call Dc.new options[:idle].merge(delay: 10)
  def dc(_options = nil, **kw)
    _options ||= options[:dc]
    ctl = Control::Dc.new c, _options.merge(kw)
    ctl.dc
  end

  def logger
    c.logger
  end

  def cmd(key, opts = {})
    c.send key, options[key].merge(opts)
  end

  def bell
    3.times do
      system 'tput bel'
      sleep 0.2
    end
  end
end

class Dumpster
  attr_reader :dir

  def initialize(dir)
    @dir = dir
    FileUtils.mkdir_p @dir
  end

  def iter
    read('iter').to_i rescue 0
  end

  def save(&block)
    n = iter + 1
    dump = []
    # `dump` can be memory consuming, but this ensures that iter
    # is written only if there is no exception.
    # `dump` can be something that defines << operator and writes
    # to disc instantly.
    yield n, dump
    dump.each { |name, s| write name, s }
    write 'iter', n
  end

  def fp(name)
    File.join dir, name
  end

  def write(name, obj)
    File.open(fp(name), 'w') { |f| f.puts obj }
  end

  def read(name)
    File.read fp(name)
  end

  def exist?(name)
    File.exist? fp(name)
  end

  def glob(pattern)
    Dir.glob pattern, base: dir
  end
end

def _experiment(options = {}, &block)
  dir = options[:dir]
  dir = File.join('experiments', dir) unless File.absolute_path? dir
  dumpster = Dumpster.new dir
  if options.has_key? :dev
    port = SerialPort.new options[:dev], 38400
    com = Com.new port
    com.gnd = options[:gnd]
    com.mils = options[:mils]
    com.motor options[:motor]
    com.fc = options[:fc]
    ampfu = options[:ampfu]
    if ampfu.has_key? :cmd
      com.ampfu = [ampfu[:fu]] + ampfu[:cmd]
    else
      # Hash is ordered.
      com.ampfu = ampfu.values
    end
  end
  e = Experiment.new(com, dumpster, options)
  e.plot.converters << UnitsConverter.new
  e.plot.converters << AddonsConverter.new
  e.instance_eval &block
end

def experiment(scheme, options = nil, &block)
  _options = {}
  # For compatibilty with *-cmd.rb.
  if options.nil?
    _options = scheme
  else
    _options = simple_options scheme
  end
  _experiment _options, &block
end
