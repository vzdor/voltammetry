require_relative 'experiment'
require_relative 'arguments'

# -300:600,-100:200,... => [[-300, 600], [-100, 200], ...]
Arguments.singleton_class.defp :step do |str|
  ary = str.split ','
  ary.map { |el| el.split(':').map &:to_i }
end

scheme = Arguments.defaults do
  {
    dc: {},
    step: step
  }
end

experiment(scheme, title: 'Step and record.') do
  plot.new_pary

  plot_thread = Thread.new do
    renderer = Plot::Renderer.new gp, plot
    loop do
      sleep 1
      renderer.render
    end
  end

  t0 = Time.now
  options[:step].each do |v, delay|
    tell "#{delay} seconds at #{v} mV."
    (0..delay).each do
      dc v: v, delay: 1
      *, vals = c.stop_done
      plot.pary.add Time.now - t0, vals.amp
    end
  end
  plot_thread.kill

  save
  tell 'Done.'
  bell
end
