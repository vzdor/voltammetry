void delay_10us(uint32_t);

#define delay_ms(ms) { delay_10us((uint32_t) ms * 100); }
