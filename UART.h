#include <avr/io.h>
#include <stdio.h>
#include <util/setbaud.h>

void UART_init();

int UART_put(char c, FILE *s);
int UART_get(FILE *s);
int UART_any();
