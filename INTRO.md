## Spec/Install

![Electrode holder with motor and PCB](docs/pcb-holder.png)

* The device is powered from computer USB, but can be powered from power supply, GND and Vin pins
* Staircase, square wave voltammetry limited to 50 mV pulse in the code
* step (dv) > 10 mV
* Error in dv > 3 mV
* Maximum current is limited by R I_max = WE. Where R = R3||R6 or R = R6
* Min recognizable current is R I > 5 V/2^ADC_bits = 5 mV. > 0.3 uA with 20k R. `--osn n` argument for higher resolution using oversampling. (implemented only for square cycling) `--osn n` deleted in master (git checkout 4d56d48).
* R3 can be switched on/off with "--mils yes" argument
* WE and CE potentials: 50 mV to USB voltage - 50 mV - Diode drop (no diode voltage drop, if powered from power supply).

### Directory structure

* top directory - atmega328 micro controller code
* pcb - device schematic, pdf for printer toner transfer and PCB source for editing with gEDA
* motor-pcb - voltage follower for PWM controlling vibrator motor
* utils - directory with library and utilities for device control.

## Installation

### Micro controller

```
apt-get install avr-gcc avr-libc avrdude
make
make TTY=/dev/ttyUSB0 flash
```

### Utils/control commands

```
apt-get install ruby bundler gnuplot-qt
cd utils
bundle
```

## Usage

### Cyclic voltammetry

```
ruby cycle-cmd.rb \
     --dir jun-nicotine \
     --dev /dev/ttyUSB0 \
     --dc-v 1200 --dc-delay 10 \
     --cycle-v1 -300 --cycle-v2 1200 \
     --cycle-delay 100 --cycle-dv 50 \
     --cycle-count 3
```

Will run staircase analysis and save plot to experiments/jun-nicotine directory.

The WE electrode is locked at 1/2 Vcc potential, CE electrode potential changes. Potential of WE can be set with --gnd k option, WE will be set to Vcc * k.

--dc-v - before cycling, keep REF = 1200 mV + WE. CE electrode will be adjusted.

--dc-delay - wait 10 seconds at specified potential before cycling.

--cycle-delay - 100 ms delay between steps.

--cycle-dv - step potential.

The first ttyUSB* device will be used if --dev argument is omitted.

### DC

```
ruby dc-cmd.rb --dc-v -200
```
Same as `--dc-v` in cycle-cmd.

```
ruby oxidize-cmd.rb --dir cuo --p -500:60,-320:120
```

Same as dc-cmd, but accepts a list of potentials. -500 mV for 60 seconds, etc. Will plot current by time and save the plot and point arrays to experiments/cuo directory.

### Stripping analysis

```
ruby strip-cmd.rb \
     --dir jul-22 \
     --dc-delay 180 --dc-v 1800 \
     --strip-v1 200 \
     --strip-delay 50 --strip-dv 30 \
     --strip-fun 1
```

--strip- - same as `--cycle-` options in cycling voltammetry.

--strip-fun - 1 is for square voltammetry. 0 for staircase. Default is 1.

Algorithm:

* Wait a few seconds at `--dc-v` potential and collect background
* Switch on the motor, switch on "mils" for higher current ability. Wait --dc-delay seconds at `--dc-v`
* Turn off motor and "mils"
* Collect points
* Subtract background.

To combine stripping plots, as on the picture at the bottom:
```
ruby plot-cmd.rb --dir jul-22 --combine 1,2
```
Will save combined.png to experiments/jul-22.

All commands accept --gnd, --motor and --mils arguments.

![Pb-Cu PGE ref/ce/cu in Acetic Acid](docs/pb-cu.png)
