#include <avr/io.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "UART.h"
#include "delay.h"

// ADC channels.
#define CE_CH 0
#define GND_CH 1
#define REF_CH 2
#define REF11_CH (1 << MUX3) | (1 << MUX2) | (1 << MUX1)

double vcc;
double ce_v; // Counter electrode voltage.
double v_min, v_max; // Min and max voltage op amp can output.
double gnd_v; // Ground/WE electrode voltage.
double ref_v; // Reference electrode voltage.
double amp_v; // Milliamps.

#define CE_PORT PORT4 // CE port number. DDB{CE_PORT} and PORTB{CE_PORT}.
#define GND_PORT PORT3 // GND port number.

extern void (*ampfu)();
void ampfu_init();
void ampfu_cmd();
void ampfu_sample();

void adc_set_channel(int c) {
    ADMUX &= ~((1 << MUX0) | (1 << MUX1) | (1 << MUX2) | (1 << MUX3));
    ADMUX |= c;
}

/* In single conversion mode, read from previously set channel
   and return ADC value. */
int adc_single_conv() {
    // Start single conversion.
    ADCSRA |= (1 << ADSC);
    // Wait for conversion to finish.
    loop_until_bit_is_clear(ADCSRA, ADSC);
    return ADCL | (ADCH << 8);
}

/* Read and return ADC value in volts relative to vcc. */
double adc_get_v() {
    return vcc/1024 * adc_single_conv();
}

void adc_init() {
    // Set prescalar to 128 for 125k ADC frequency.
    ADCSRA |= (1 << ADPS0) | (1 << ADPS1) | (1 << ADPS2);
    // Set ADC HREF voltage to avcc which is connected to Vcc in Nano.
    ADMUX |= (1 << REFS0);
    // Enable ADC.
    ADCSRA |= (1 << ADEN);
}

/* RC circuit: Port - R - C - GND.
   If V(C) > v, set port to low (GND) for time found by solving RC
   circuit for t. This will discharge C to v.
   If V(C) < v, set port to high (Vcc) for time t.
   Set port to open circuit mode. */
void _cap_set_v(double v, double cap_v, int port) {
    double Vcc = vcc;
    uint32_t tu;

    if (cap_v > v) Vcc = 0;
    // 6600 = 330 nF * 200 kOhm * 100000, time in 10 us.
    tu = (uint32_t) (6600.0 * log((Vcc - cap_v)/(Vcc - v)));
    if (tu == 0) return;
    if (cap_v > v) {
        DDRB |= (1 << port);
        PORTB &= ~(1 << port);
    } else {
        DDRB |= (1 << port);
        PORTB |= (1 << port);
    }
    delay_10us(tu);
    // Set port to open circuit/TRI-STATE.
    DDRB &= ~(1 << port);
    PORTB &= ~(1 << port);
}

void ce_set_v(double v) {
    _cap_set_v(v, ce_v, CE_PORT);
}

void gnd_set_v(double v) {
    _cap_set_v(v, gnd_v, GND_PORT);
}

/* In command calls, quit the command if any input from UART. */
inline int cmd_op() {
    return !UART_any();
}

static void acquire_vcc() {
    // Read from internal ADC 1.1v ref to calculate vcc.
    adc_set_channel(REF11_CH);
    adc_single_conv();
    // Wait for filtering caps on init and ignore first reading
    // as, from DS, it can be incorrect.
    delay_ms(300);
    vcc = 1024 * 1.1/adc_single_conv();
}

static void motor(int on, int dc) {
    DDRD |= (1 << DDD6); // PD6 as output.
    if (on) {
        OCR0A = dc; // set PWM duty cycle.
        TCCR0A |= (1 << COM0A1);
#ifdef INVERTING_MOTOR_PWM
        TCCR0A |= (1 << COM0A0);
#endif
        TCCR0A |= (1 << WGM01) | (1 << WGM00); // fast PWM Mode.
        // set No prescaling. f_PWM = F_CPU/(1 * (1 + 255) = 62.5 kHz.
        TCCR0B |= (1 << CS00);
    } else {
        // To prevent current increase, try to stop PWM at GND
        // (in inverting config).
        OCR0A = 0;
        TCCR0B = 0;
        TCCR0A = 0;
#ifdef INVERTING_MOTOR_PWM
        PORTD |= (1 << PD6);
#else
        PORTD &= ~(1 << PD6);
#endif
    }
    // The current that motor draws affects vcc potential, if
    // powered from Nano 5 V pin.
    acquire_vcc();
}

inline void acquire_gnd();
inline void acquire_ce();

static void init() {
    UART_init();
    adc_init();
    motor(0, 50); // Stop motor.
    acquire_vcc();
    v_min = 0.05;
    v_max = vcc - 0.05;
    acquire_gnd();
    gnd_set_v(vcc/2);
    acquire_ce();
    ce_set_v(vcc/2);
}

inline void acquire_ref() {
    adc_set_channel(REF_CH);
    ref_v = adc_get_v(REF_CH);
}

inline void acquire_ce() {
    adc_set_channel(CE_CH);
    ce_v = adc_get_v();
}

inline void acquire_gnd() {
    adc_set_channel(GND_CH);
    gnd_v = adc_get_v();
}

void acquire() {
    acquire_gnd();
    (*ampfu)();
    // ampfu_sample();
    acquire_ref();
    acquire_ce();
}

static void print_given_values(double _ce_v, double _gnd_v, double _ref_v,
                               double _amp_v) {
    const int f = 1000; // Volts to millivolts.
    printf("%d %d %d %d\n",
           (int)(_ce_v * f), (int)(_gnd_v * f), (int)(_ref_v * f),
           (int)(_amp_v * f * 10)); // in millis already, frk.
}

static inline void print_values() {
    print_given_values(ce_v, gnd_v, ref_v, amp_v);
}

static void _staircase(double v, double dv, int _delay_ms, int add[]) {
    double *less = &ce_v, *than = &v, _dv = dv;

    if (ce_v > v) {
        less = &v;
        than = &ce_v;
        _dv = -dv;
    }
    while (*less < *than && cmd_op()) {
        acquire();
        ce_set_v(ce_v + _dv);
        print_values();
        delay_ms(_delay_ms);
    }
}

static void _square(double v, double dv, int _delay_ms, int add[]) {
    const int tp = _delay_ms/2; // Time of pulse.
    float ep = 0.05; // E of pulse.
    double f_ce, f_gnd, f_ref, f_amp; // Forward pulse values.

    if (add[0] > 0)
        ep = add[0]/1000.0;

    // gnd and ce is up to date by preceded dc_cmd.
    // acquire_gnd();
    // acquire_ce();

    while (ce_v > v && cmd_op()) {
        // Forward pulse.
        ce_set_v(ce_v - dv - ep);
        delay_ms(tp);
        acquire();
        f_amp = amp_v;
        f_ce = ce_v;
        f_gnd = gnd_v;
        f_ref = ref_v;
        // Backward pulse.
        ce_set_v(ce_v + ep);
        delay_ms(tp);
        acquire();
        print_given_values(f_ce, f_gnd, f_ref, f_amp - amp_v);
    }
}

#define ITER_FORMAT "# iter %d\n"

static void cycle_cmd() {
    double dv, v1, v2;
    int fun, _dv, delay, cycles, _v1, _v2;
    int i;
    void (*funp) (double, double, int, int[]);
    const int add_len = 1;
    int add[add_len];

    scanf(" %d %d %d %d %d %d", &fun, &_dv, &delay, &cycles, &_v1, &_v2);

    dv = _dv/1000.0;
    v1 = _v1/1000.0;
    v2 = _v2/1000.0;

    // Read additional, function specific arguments.
    for (i = 0; i < add_len; i++) scanf(" %d", &add[i]);

    if (fun == 1)
        funp = &_square;
    else
        funp = &_staircase;
    if (cycles == 0) {
        printf(ITER_FORMAT, 0);
        (*funp)(v1, dv, delay, add);
    }
    for (i = 0; i < cycles && cmd_op(); i++) {
        printf(ITER_FORMAT, i);
        (*funp)(v1, dv, delay, add);
        (*funp)(v2, dv, delay, add);
    }
    printf("# done\n");
}

static void dc_cmd(void (*funp) (double)) {
    int _v, err = 1;
    double v;

    scanf(" %d", &_v);
    v = (double) _v/1000.0;

    if (v < 0)
        v = v_min;
    else if (v > v_max)
        v = v_max;
    else
        err = 0;

    if (err)
        printf("# err too high/low, adjusted.\n");

#if RC_TOLERANCE > 1
    /* See _cap_set_v. The equation for calculating time:
       t = RC log((Vcc - v1)/(Vcc - v2))

       But actually: |RC - (RC)_components| > 0.

       If _cap_set_v will be called a few times with the same value,
       will this reduce the error in voltage?


       Equation with RC components error:
       t = (RC + RC * tol) log((Vcc - v1)/(Vcc - v2))

       Solving for v2 and assuming v2 > v1, so evaluating at Vcc = 0.

       v2 = v1 e^-(t/(RC + RC tol)).
       With tol = 0, v2' = v1 e^-(t/(RC)).

       Assuming t = RC, the error in v2:
         v2 - v2' = v1 (e^-(1/(tol + 1)) - e^-1)

       If tol is 1/10, then the maximum error in v2 is 0.035 v1. The
       maximum error is at t = RC.

       1. 0.035 v1
       2. 0.035 (0.035 vl) = 0.00122 v1
    */
    (*funp)(v);
    acquire();
    (*funp)(v);
    acquire();
#endif
    do {
        (*funp)(v);
        acquire();
        print_values();
        // Allow quit before delay_ms. TODO: Add 'set once' option.
        if (cmd_op()) delay_ms(1000);
    } while (cmd_op());

    printf("# done\n");
}

static void i_cmd() {
    printf("# init vcc %d\n", (int) (vcc * 1000));
    acquire();
    print_values();
    printf("# done\n");
}

static void motor_cmd() {
    int on, dc;
    scanf(" %d %d", &on, &dc);
    motor(on, dc);
    printf("# done\n");
}

FILE _out = FDEV_SETUP_STREAM(UART_put, NULL, _FDEV_SETUP_WRITE);
FILE _in = FDEV_SETUP_STREAM(NULL, UART_get, _FDEV_SETUP_READ);

int main() {
    char c;

    stdout = &_out;
    stdin = &_in;

    init();
    ampfu_init();
    i_cmd();

    while (1) {
        scanf(" %c", &c);
        switch (c) {
        case 'c':
            cycle_cmd();
            break;
        case 'v':
            dc_cmd(&ce_set_v);
            break;
        case 'g':
            dc_cmd(&gnd_set_v);
            break;
        case 'i':
            i_cmd();
            break;
        case 'p':
            motor_cmd();
            break;
        case 'f':
            ampfu_cmd();
            break;
        default:
            printf("# err unknown command '%c'\n", c);
        }
    }
    return 0;
}
