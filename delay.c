#include <util/delay.h>
#include "delay.h"

#ifndef DELAY_K
#warning Set DELAY_K to integer that divides F_CPU, to reduce delay_10us() error.
#endif

/* Delay for n * 10 us.
   If n is zero, delay for _delay_loop_2(0), which is 2^16 cycles. */
void delay_10us(uint32_t n) {
    // _delay_loop_2(1) will sleep 4 cycles. _delay_loop_2(1)'s in 1 s:
    const uint32_t cps = F_CPU/4;
    // The argument to _delay_loop_2 is unit16_t, make sure there
    // is no overflow.
    const uint32_t nps = 100000, // number of n's in 1 s.
#ifdef DELAY_K
	k = DELAY_K;
#else
        // 16 MHz.
	k = cps/40000; // Solve cps/k = 40000 for k.
#endif

    // _delay_loop_2(cps/k) is the same as delay_10us(nps/k).

    for (; n > nps/k; n -= nps/k) {
        _delay_loop_2(cps/k);
    }
    // n * cps/nps should be less than 65536.
    _delay_loop_2(n * cps/nps);
}
