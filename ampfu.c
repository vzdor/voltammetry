#include <avr/io.h>
#include <stdio.h>

// Amp reading definitions and amp commands.

extern double gnd_v;
extern double amp_v;

double adc_get_v();
void adc_set_channel(int channel);

// Public/interface.

void ampfu_init();
void ampfu_sample_channel_set();
void ampfu_sample();
void (*ampfu)() = ampfu_sample;
void ampfu_cmd();

// You can add your function to this array, see example below.
void (*ampfu_commands[5])();
int ampfu_commands_len = 0;

// Private/impl.

#define AMP_CH 3
#define FRS_PORT PORT0 // R3 off/on.

static double frk = FRK; // TIA feedback resistor value, in k.
static int amp_channel = AMP_CH; // AMP (or filter) channel.

void ampfu_sample_channel_set() {
    double v = adc_get_v();
    amp_v = (v - gnd_v)/frk;
}

void ampfu_sample() {
    adc_set_channel(amp_channel);
    ampfu_sample_channel_set();
}

static void amp_channel_cmd() {
    int channel;
    scanf(" %d", &channel);
    if (channel < 3 || channel > 5) {
        printf("# err\n");
    } else {
        amp_channel = channel;
        printf("# done\n");
    }
}

static void sfr(int on) {
    DDRB |= (1 << FRS_PORT);
    if (on) {
        PORTB |= (1 << FRS_PORT);
        frk = FRK/(1.0 + FRK); // Two R's in parallel.
    } else {
        PORTB &= ~(1 << FRS_PORT);
        frk = FRK;
    }
}

static void sfr_cmd() {
    int on;
    scanf(" %d", &on);
    sfr(on);
    printf("# done\n");
}

static void do_ampfu_command(int cmd) {
    if (cmd >= ampfu_commands_len)
        printf("# err fu\n");
    else
        (*ampfu_commands[cmd])();
}

void ampfu_cmd() {
    int cmd;
    scanf(" %d", &cmd);
    if (cmd == 0) {
        ampfu = ampfu_sample;
        printf("# done\n");
    } else if (cmd == 1)
        amp_channel_cmd();
    else if (cmd == 2)
        sfr_cmd();
    else if (cmd >= 10)
        do_ampfu_command(cmd - 10);
    else
        printf("# err fu\n");
}

void ampfu_init() {
    sfr(0);
}

// Some amp function examples.

static int samples_number;

static void sampling_ampfu() {
    double v = 0;
    int i;
    ampfu_sample();
    v += amp_v;
    for (i = 1; i < samples_number; i++) {
        ampfu_sample_channel_set();
        v += amp_v;
    }
    amp_v = v/samples_number;
}

static void sampling_ampfu_cmd() {
    int num;
    scanf(" %d", &num);
    samples_number = num;
    ampfu = sampling_ampfu;
    printf("# done\n");
}

static int ips_number;

static void sampling_ips_ampfu() {
    int i;
    for (i = 0; i < ips_number; i++) {
        sampling_ampfu();
        printf("# ip %04d\n", (int)(amp_v * 10000));
    }
}

static void sampling_ips_ampfu_cmd() {
    int samples;
    int num;
    scanf(" %d %d", &samples, &num);
    samples_number = samples;
    ips_number = num;
    ampfu = sampling_ips_ampfu;
    printf("# done\n");
}

void example_ampfu_init() __attribute__((constructor));
void example_ampfu_init() {
    ampfu_commands[ampfu_commands_len] = sampling_ampfu_cmd;
    ampfu_commands_len ++;
    ampfu_commands[ampfu_commands_len] = sampling_ips_ampfu_cmd;
    ampfu_commands_len ++;
}
