## DSL

I want to subtract background from voltammogram. I do not want to add this to plot-cmd since it is a special case. Instead, here is an example of plot library.

```ruby
require_relative 'experiment'
require_relative 'arguments'
require_relative 'pary_files'
require_relative 'converters/all'

# Define argument defaults, you can overwrite them from
# command line.
scheme = Arguments.defaults do
  {
    dev: optional(:device),
    combine: selector,
  }
end

experiment(scheme, title: 'DSL1.') do
  s = SelectConverter.new
  # Filter points at -0.4 +/- 0.15 v.
  s.add :at, -400, 150
  # Apply simple moving median.
  s.add :avg, 1
  # Add converters to plot. Plot#to_s will run them.
  plot.converters << s
  # [1, 2, 3, 4] -> [[1, 2], [3, 4]].
  # Next step will be cleaner with pairs.
  s.add_proc { |parys| parys.each_slice 2 }
  # [[1, 2], [3, 4]] -> [5, 6]
  s.add_map do |pary1, pary2|
    # Make new empty plot array, clone style variable.
    final = pary2.empty.copy! :style
    p1 = pary1.max_by &:y
    # For each index, n, in array
    pary1.each_index do |n|
      p2 = pary2[n]
      break unless p2
      di = p2.y - p1.y
      next if di < 0
      final.add p2.x, di
    end
    final
  end

  pf = ParyFiles.new dumpster
  plot.parys.concat pf.all(options[:combine])
  # Show gnuplot window.
  gp.puts plot.to_s
end
```

To add plot styles:
```ruby
s.add_proc do |parys|
  p1, p2, p3 = parys
  p1.style.lines dt: 2
  p2.style.linespoints ps: 1
  p3.style.lines color: 'black', lw: 2
  parys
end
```

`dt, ps, lw` - gnuplot style attributes.

To addess arrays by title or file-id:
```ruby
s.add(:select, ['8-0.']).add_map do |pary|
  pary.style.lines color: 'red'
  pary
end
```

To set font, gnuplot key/setting, linetypes:
```ruby
plot.settings.terminal.font = 'Latin Modern Mono,10'
plot.settings.set :key, 'outside'
plot.settings.linetype 1, lw: 2
plot.settings.linetype 2, lw: 2, dt: 2
plot.settings.colors_16
```
