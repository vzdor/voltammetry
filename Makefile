MCU=atmega328p
TTY=/dev/ttyUSB0
BAUD=38400
FRK=20
RC_TOLERANCE=10
INVERTING_MOTOR_PWM=1

%.o: %.c
	avr-gcc -Wfloat-conversion -Os -DBAUD=$(BAUD) -DF_CPU=16000000UL -DFRK=$(FRK) -DRC_TOLERANCE=$(RC_TOLERANCE) -DINVERTING_MOTOR_PWM=$(INVERTING_MOTOR_PWM) -mmcu=$(MCU) -c -o $@ $<

meter2.elf: UART.o delay.o meter2.o ampfu.o
	avr-gcc -mmcu=$(MCU) -o $@ $^

meter2.hex: meter2.elf
	avr-objcopy -O ihex -R .eeprom $< $@

flash: meter2.hex
	# -b 57600 for Nano bootloader.
	avrdude -c arduino -p $(MCU) -P $(TTY) -b 57600 -U flash:w:$<

stty:
	stty -F $(TTY) hupcl raw icanon eof \^d $(BAUD)

clean:
	rm -f *.hex *.elf *.o
