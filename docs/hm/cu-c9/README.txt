Electrolyte: 4 ml water, 1 ml A.A. 9% by weight.

1 - cycle, 500 ms. (error, took longer time.)
2 - dep.
3 - electrodes in electrolyte, sleep 600 s at -300 mV, cycle.
4 - dep.

5 - new el, cycle.
6 - dep.
7 - sleep 600 s, cycle. (error, tool longer time.)
8 - dep.

Add 1 drop of iodine.

9 - new el, cycle.
10 - dep.
11 - sleep 600s, cycle.
12 - dep.

Add 4 drops  2 ug/ml Cu.

13 - new el, cycle.
14 - dep.
15 - sleep 600 s, cycle.
16 - dep.

New elec, add 5 drops of Cu.

17 - dep, new wires.
18 - cycle from -870 to 0, 500 ms.
19 - dep.

20 - new el, dep.
21 - cycle from -870 to 0.
22 - dep.
# 23 - dep, fix electrode clip, error: ctrl-c too soon.
23 - cycle.

Add 1 drop iod.

24 - new el, dep.
25 - cycle from -870 to 0.
26 - dep.
27 - cycle.