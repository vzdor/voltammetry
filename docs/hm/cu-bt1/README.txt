* Tap water
  Copy from cu-nt4 5, 6 -> 1, 2.

  0.5 C.A. 1 g/50 ml 2.25 g NaCl. 1 drop iod.

  1 - dep.
  2 - dep, new el, add 0.1 Pb.

* Bic river water
  Copy from cu-bic5 1, 5 -> 3, 5.

** Bic water processing
  15 May, 20:00.
  5 ml Bic water, 0.3 ml C.A 1 g/50 ml 3 g NaCl, 2 drops hydrogen peroxide.

  16 May. 14:00.
  Move 2.5 ml to a new bottle.

  To decompose Hyd. Per., add KMnO4 untill dark brown color, C.A. and Hyd. per. reacted.

  Add 0.3 C.A. without NaCl, solution turns transparent.
  Add 1 drop iod.

  3 - dep.
  5 - dep, new el.

* Plotting
  ruby plot-cmd.rb --dir cu-bt1 --combine 1.. --draw lines --smm 1 --titles 'tap, tap + 3 ug/l Pb, Bic, Bic + 3 ug/l Pb' --plot-font 'Latin Modern Mono,10'
