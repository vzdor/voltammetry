## Filtering

### HW filters

ADC channel can be selected with `--fc n`. `n` can be 3 or 4. The filter can be added between A3 and A4 pins.

### Digital filtering

`utils/plot-cmd.rb --filter` will apply 4 degree lowpass FIR filter to staircase (SC) and DFT filter to square wave voltammograms. When voltammogram is recorded, dt, dv, v1, v2 options and type of voltammogram will be saved too. The "filter" use these options to decide which filter to apply. The "filter" will split SC voltrammogram by v1 and filter sides individually, to try to keep the shape of corner points. FIR filter is selected from the defined table of filters, dv/dt is used in assumption which FIR filter to select, see utils/ft-filter/README. The table of filters were made using `dfcgen` free program (`apt-get install dfcgen-gtk`). `--filter-cutoff n` allows limiting filters table to filters below `n`. If voltammogram looks noisy, `n` can be lowered. `n` can vary from 30 to 2.

`utils/ft-filter/plot-cmd.rb` can be used to view frequency spectrum.
