v 20201211 2
C -900 700 1 0 0 gnd-1.sym
C -1100 1000 1 0 0 vac-1.sym
{
T -500 1950 5 10 1 1 0 0 1
refdes=V1
T -400 1850 5 10 0 0 0 0 1
device=vac
T -400 2050 5 10 0 0 0 0 1
footprint=none
T -600 850 5 10 1 1 0 0 1
value=PWL(0 300mV 10us 250mV 50ms)
}
C 1200 2100 1 0 0 resistor-2.sym
{
T 1600 2450 5 10 0 0 0 0 1
device=RESISTOR
T 1400 2400 5 10 1 1 0 0 1
refdes=R3
T 1300 1800 5 10 1 1 0 0 1
value=3k
}
C 3100 2000 1 0 0 capacitor-1.sym
{
T 3300 2700 5 10 0 0 0 0 1
device=CAPACITOR
T 3200 2400 5 10 1 1 0 0 1
refdes=C1
T 3300 2900 5 10 0 0 0 0 1
symversion=0.1
T 3000 1800 5 10 1 1 0 0 1
value=3.3uF
}
C 3900 1900 1 0 0 gnd-1.sym
C 3100 3000 1 0 0 resistor-2.sym
{
T 3500 3350 5 10 0 0 0 0 1
device=RESISTOR
T 3300 3300 5 10 1 1 0 0 1
refdes=R2
T 3200 2700 5 10 1 1 0 0 1
value=300k
}
N 3100 3100 3100 2200 4
N 4000 2200 4000 3100 4
C -600 2500 1 0 0 spice-options-1.sym
{
T -500 2800 5 10 0 1 0 0 1
device=options
T -500 2900 5 10 1 1 0 0 1
refdes=Z1
T -500 2600 5 10 1 1 0 0 1
value=savecurrents
}
C -300 1500 1 0 0 vcvs-1.sym
{
T -100 2550 5 10 0 0 0 0 1
device=SPICE-vcvs
T 300 2350 5 10 1 1 0 0 1
refdes=E1
T -100 2750 5 10 0 0 0 0 1
symversion=0.1
T 400 1450 5 10 1 0 0 5 1
value=100k
}
N -800 2200 -300 2200 4
C 2100 2100 1 0 0 resistor-2.sym
{
T 2500 2450 5 10 0 0 0 0 1
device=RESISTOR
T 2300 2400 5 10 1 1 0 0 1
refdes=R1
T 2200 1800 5 10 1 1 0 0 1
value=15k
}
N 3000 2200 3100 2200 4
N -300 1600 -300 1100 4
N -300 1100 2100 1100 4
{
T 1600 1250 5 10 1 1 0 0 1
netname=Ref
}
N 2100 1100 2100 2200 4
C 1100 1300 1 0 0 gnd-1.sym
