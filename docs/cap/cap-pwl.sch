v 20201211 2
C 1300 700 1 0 0 gnd-1.sym
C 1100 1000 1 0 0 vac-1.sym
{
T 1600 1950 5 10 1 1 0 0 1
refdes=V1
T 1800 1850 5 10 0 0 0 0 1
device=vac
T 1800 2050 5 10 0 0 0 0 1
footprint=none
T 1700 1150 5 10 1 1 0 0 1
value=PWL(0 300mV 10us 250mV 50ms)
}
N 1400 2200 2100 2200 4
C 2100 2100 1 0 0 resistor-2.sym
{
T 2500 2450 5 10 0 0 0 0 1
device=RESISTOR
T 2300 2400 5 10 1 1 0 0 1
refdes=R1
T 2200 1800 5 10 1 1 0 0 1
value=15k
}
C 3400 2000 1 0 0 capacitor-1.sym
{
T 3600 2700 5 10 0 0 0 0 1
device=CAPACITOR
T 3500 2400 5 10 1 1 0 0 1
refdes=C1
T 3600 2900 5 10 0 0 0 0 1
symversion=0.1
T 3300 1800 5 10 1 1 0 0 1
value=3.3uF
}
C 4200 1900 1 0 0 gnd-1.sym
N 3000 2200 3400 2200 4
C 3400 3000 1 0 0 resistor-2.sym
{
T 3800 3350 5 10 0 0 0 0 1
device=RESISTOR
T 3600 3300 5 10 1 1 0 0 1
refdes=R2
T 3500 2700 5 10 1 1 0 0 1
value=300k
}
N 3400 3100 3400 2200 4
N 4300 2200 4300 3100 4
C 1300 2600 1 0 0 spice-options-1.sym
{
T 1400 2900 5 10 0 1 0 0 1
device=options
T 1400 3000 5 10 1 1 0 0 1
refdes=Z1
T 1400 2700 5 10 1 1 0 0 1
value=savecurrents
}
