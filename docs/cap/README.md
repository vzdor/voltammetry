## Ngspice and capacitance

I simulate staircase potential applied to simplified Randles circuit in Ngspice, to understand how capacitance can be estimated from voltammogram. I use `plot-cmd.rb` to display Ngspice simulation. I assume there is no Faradaic process or it is negligible, Rr (R2) >> Rc (R1).

![Circuit](cap-pwl.png)

REF and CE wires connected to R1 from the side of V1. WE is connected to GND.

To record voltammogram:
```
ruby cycle-cmd.rb --dc-v 300 --idle
ruby cycle-cmd.rb --dir cap2-pwl --cycle-dv 50 --cycle-delay 45 --cycle-v1 1000 --cycle-v2 300 --cycle-count 1 --dc-v 300
```

The first, `--idle` command to set WE at -0.3 V since it is polar capacitor.

![Voltammogram and ngspice PWL simulation](3uF-sim.png)


1-0 is recorded voltammogram. i-2-0 plots were made by simulation in ngspice circuit simulator using PWL (Piece-Wise linear) to produce staircase voltage. To construct PWL string (vector in ngspice):
```
ruby spice/pwl-cmd.rb --sc-v0 -1000 --sc-dv -50 --sc-n 5
# Will print:
ALTER @v1[pwl] = [ 0ms -1000mV 0.01ms -950mV 50ms -950mV 50.01ms -900mV 100ms -900mV 100.01ms -850mV 150ms -850mV 150.01ms -800mV 200ms -800mV 200.01ms -750mV 250ms -750mV ]
```

`--sc-n` is the number of steps.

To save simulation, from ngspice console:
```
print i(v1) + @r2[i], i(v1), @r2[i], @c1[i] > i-2-0.spice
```

To render the spice file and the recorded voltammogram:
```
ruby plot-cmd.rb --dir cap2-pwl --combine 1,2 --view --plot-x2y1 "1-0." --smm 1 --plot-commands "key opaque, x2label 'V', xlabel 't', xtics norotate"
```

`--smm` option to delete voltammogram noise. There is no option to set plot styles individually, I did this manually.

Capacitance is defined as: C = q/V.

q = integrate(i(t), t0, t1). t1 - t0 = dt, step time (`--cycle-dt`). V is step voltage (`--cycle-dv`). If we look at @c1[i] plot, integral(i) or q is close to: 1.7 uA dt + HR. Where 1.7 uA is value of i on the voltammogram at some v or the bottom value of @c1[i]. HR is half of the rectangle: ((i0 + 1.7) - 1.7 uA) dt/2. Recall capacitor charging equation: i = V/R e^-(t/(RC)). At t = 0, i0 = V/R. i0 = 0.05 V/(15 10^3 Ohm) = 5/3 uA. So C = q/dv = q 50ms/50mV = 3.36 uF.
```
C = (i + dv/(2R1)) dt/dv   (1)
```

### R1 is not known

Assume we found two different pairs of (dt, dv) so that (1) gives same capacitance, C(dt1, i1) = C(dt2, i2). Solving for R and substituting R in C(dt1, i1):
```
C = - dt1 dt2 (i2 - i1)/((dt2 - dt1) dv)   (2)
```

If dt1 = dt2/2 and dv = dt2: C = (i1 - i2) s/v.

### Too fast

![dt is too small](50-25ms-3-6uF-v.png)

v-3 simulation is done with 3.3 uF, 50 ms steps. v-4 is 6.8 uF, 25 ms. v-4 plot slopes differ, so voltammogram will be tilted. In practice, increment step time until there is no tilt. Below I find relation between dt and number of points until horizontal plot.

RC equation, from V0 to E:
```
                        t
                     - ---
                       C R
(%o2) V = (V0 - E) %e      + E
```

V -- voltage on capacitor. Will use Maxima (free/gnu) for algebraic manipulations and analysis.

To address E by step number, substitute `dv n` for E:
```
                           t
                        - ---
                          C R
(%o3) V = (V0 - dv n) %e      + dv n
```

For simplicity, set t = k RC.
```
        - k
(%o5) %e    (V0 - dv n) + dv n
```

To find V at n + 1, substitute V0 = V(n), n = n + 1. `eq` is previous equation. The result is long expression and I do not show it. I want to know how slope of V changes with n, that is why diff:
```
(%i7) eq1 : ev(eq, V0=eq, n=n + 1);
(%i8) eqd1 : diff(eq1, n);
                - 2 k
(%o8) dv - dv %e
```

Will do the same for n = 3, 8, I omit some eqx steps:
```
(%i11) eq3: ev(eq, V0=eq2, n=n + 1);
(%i12) diff(eq3, n);
                 - 4 k
(%o12) dv - dv %e
(%i17) eq8: ev(eq, V0=eq7, n=n + 1);
(%i18) diff(eq8, n);
                 - 9 k
(%o18) dv - dv %e
```

It looks like the slope of V is:
```
                 - k n
(%o19) dv - dv %e
```

I would like to know at what n the slope of V will be close to staircase voltage slope, dv. In this case i will be same each step, so no voltammogram tilt. Lets say, less than 2% difference from staircase slope is close enough.
```
            - k n
(%o20) dv %e      < 0.02 dv
(%i26) ev(%, n=4/k);
         - 4
(%o26) %e    dv < 0.02 dv
(%i27) float(%);
(%o27) 0.01831563888873418 dv < 0.02 dv
```

After n = 4/k steps, slope of V will be 2% close to the slope of staircase.

Recall that t = k RC or k = t/RC. If t = RC, k = 1; if t = RC/2, k = 1/2, etc. Where t is dt (`--cycle-delay`). For dt = RC/2, will need n = 4/(1/2) = 8 steps before horizontal voltammogram. If voltage step, dv, `--cycle-dv`, is 50 mV, will only see horizontal line at 50 mV/step 8 steps = 400 mV. In order to fit in voltage window < 400 mV, I will have to increase dt or decrease dv.

```
RC = n t/4   (3)
```
Where n is the number of points until horizontal line. t = dt.

Recall HR integral and previous section. HR will have an error if line from i(t0) to i(t1) is bent. With dt = RC the line will be bent. Select dt so that there is more than 4 points until horizontal line, dt < RC.

### Effect of R2

Solving for capacitor voltage, V0 = 0:
```
                          (R2 + R1) t
                        - -----------
                            C R1 R2
        E R2     E R2 %e
(%o53) ------- - --------------------
       R2 + R1         R2 + R1
```

Effect on RC time:
```
(%i9) eq;
                                  (R2 + R1) t
(%o9)                             -----------
                                    C R1 R2
(%i10) ev(eq, R2=R1);
                                     2 t
(%o10)                               ----
                                     C R1
(%i11) ev(eq, R2=5 * R1);
                                     6 t
(%o11)                              ------
                                    5 C R1
```

### The role of reference electrode

Simulate cap-pwl-vcvs.

### spice netlist

The schematic is in gEDA format.

```
lepton-netlist -g spice-sdb cap-pwl.sch -o cap-pwl.net
```
